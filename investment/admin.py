from django.contrib import admin
from .models import Investment, Expenses, Capital

admin.site.register(Capital)
admin.site.register(Investment)
admin.site.register(Expenses)


