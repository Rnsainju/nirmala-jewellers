from django.urls import include, path

from .views import *

app_name ='investmentsandexpenses'

urlpatterns = [
    path('investments/', InvestmentListView.as_view(), name='investments'),
    path('investment/<int:pk>/',InvestmentUpdateView.as_view(), name='investment-update'),
    path('capital/', CaptalListView.as_view(), name='capital'),
    path('capital/<int:pk>/',CapitalUpdateView.as_view(), name='capital-update'),
    path('expenses/', ExpensesListView.as_view(), name='expenses'),
    path('expenses/<int:pk>/',ExpenseUpdateView.as_view(), name='expense-update'),
    path('summary/', SummaryListView.as_view(), name='summary'),
]