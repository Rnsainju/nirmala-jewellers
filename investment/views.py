from django.shortcuts import render, redirect, get_object_or_404
from django.views import View
from .models import Investment, Expenses, Capital
from .forms import InvestmentForm, ExpensesForm, CapitalForm
from django.db.models import Sum
import json
from django.db.models import Q
from django.contrib.auth.mixins import PermissionRequiredMixin
from django.views.generic import View, ListView, DetailView, TemplateView, UpdateView,DeleteView
from django.urls import reverse_lazy
from django.http import JsonResponse
from django.template.loader import render_to_string
from django.shortcuts import get_object_or_404
import nepali_datetime
from django.db.models.functions import ExtractMonth
import nepali_datetime
from inventory.models import Stock, Ornaments
from sales.models import Sales
from transaction.models import Transaction, Kaligar, Kaligarorder

class InvestmentListView(View):
    template_name = 'investment&expenses/investment_list.html'

    def get(self, request):
        form=InvestmentForm()
        investments = Investment.objects.all()
        investment_type=self.request.GET.get('investmenttype')
        if investment_type:
            investment=Investment.objects.filter(investment_type='Personel').order_by('-date')
            total=investment.aggregate(total=Sum('amount')).get('total') or 0
        else:
            investment=Investment.objects.filter(investment_type='Shop',status='approved').order_by('-date')
            total=investment.aggregate(total=Sum('amount')).get('total') or 0
            # profit=
        totalinvestment=investments.aggregate(total=Sum('amount')).get('total') or 0
        investmentshop=Investment.objects.filter(investment_type='Shop')
        totalshopinvestment=investmentshop.aggregate(total=Sum('amount')).get('total') or 0
        investmentpersonel=Investment.objects.filter(investment_type='Personel')
        totalpersonelinvestment=investmentpersonel.aggregate(total=Sum('amount')).get('total') or 0
        return render(request, self.template_name, {
            'form':form,
            'investment': investment,
            'totalshopinvestment':totalshopinvestment,
            'totalpersonelinvestment':totalpersonelinvestment,
            'totalinvestment':totalinvestment,
            'total':total,
            })

    def post(self, request):
        form = InvestmentForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect('investmentsandexpenses:investments')
        return render(request, self.template_name, {'form': form})
    
    def delete(self,request):
        id=json.loads(request.body)['id']
        investment=get_object_or_404(Investment,id=id)
        investment.delete()
        return redirect('investmentsandexpenses:investments')
    
class InvestmentUpdateView(PermissionRequiredMixin, UpdateView):
    permission_required='superuserstatus'
    model=Investment
    form_class=InvestmentForm
    template_name='investment&expenses/investment-update.html'
    success_url=reverse_lazy('investmentsandexpenses:investments')
    
    def form_valid(self, form):
        self.object = form.save(commit=False)
        self.object.save()
        return super().form_valid(form)

class ExpensesListView(PermissionRequiredMixin,View):
    model=Expenses
    permission_required='superuserstatus'
    template_name = 'investment&expenses/expenses_list.html'

    def get(self, request):
        form=ExpensesForm()
        expenses = Expenses.objects.all().order_by('-date')
        total=expenses.aggregate(total=Sum('amount')).get('total') or 0
        openingexpenses = Expenses.objects.filter(category='Shop')
        openingexpense=openingexpenses.aggregate(total=Sum('amount')).get('total') or 0
        otherexpenses = Expenses.objects.exclude(Q(category='Shop') | Q(category='Interest'))
        otherexpense=otherexpenses.aggregate(total=Sum('amount')).get('total') or 0
        bank = Expenses.objects.filter(category='Interest')
        bankexpense=bank.aggregate(total=Sum('amount')).get('total') or 0
        return render(request, self.template_name, {
            'form':form,
            'expenses': expenses,
            'total':total,
            'openingexpense':openingexpense,
            'otherexpense':otherexpense,
            'bankexpense':bankexpense
            })

    def post(self, request):
        form = ExpensesForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect('investmentsandexpenses:expenses')
        return render(request, self.template_name, {'form': form})

    def delete(self,request):
        id=json.loads(request.body)['id']
        expense=get_object_or_404(Expenses,id=id)
        expense.delete()
        return redirect('investmentandexpenses:expenses')
        
class ExpenseUpdateView(PermissionRequiredMixin, UpdateView):
    permission_required='superuserstatus'
    model=Expenses
    form_class=ExpensesForm
    template_name='investment&expenses/expenses-update.html'
    success_url=reverse_lazy('investmentsandexpenses:expenses')
    
    def form_valid(self, form):
        self.object = form.save(commit=False)
        self.object.save()
        return super().form_valid(form)

class CaptalListView(PermissionRequiredMixin,View):
    model=Capital
    permission_required='superuserstatus'
    template_name = 'investment&expenses/capital_list.html'

    def get(self, request):
        form=CapitalForm()
        capital = Capital.objects.all().order_by('-date')
        total=capital.aggregate(total=Sum('amount')).get('total') or 0
        return render(request, self.template_name, {
            'form':form,
            'capital': capital,
            'total':total,
            })

    def post(self, request):
        form = CapitalForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect('investmentsandexpenses:capital')
        return render(request, self.template_name, {'form': form})

    def delete(self,request):
        id=json.loads(request.body)['id']
        expense=get_object_or_404(Expenses,id=id)
        expense.delete()
        return redirect('investmentandexpenses:expenses')
    
    def update(self, request, expense_id):
            expense = get_object_or_404(Expenses, id=expense_id)
            if request.method == 'POST':
                form = ExpensesForm(request.POST, instance=expense)
                if form.is_valid():
                    form.save()
                    return redirect('investmentsandexpenses:expenses') 
            else:
                form = ExpensesForm(instance=expense)
            return render(request, self.template_name, {'form': form})
        
class CapitalUpdateView(PermissionRequiredMixin, UpdateView):
    permission_required='superuserstatus'
    model=Expenses
    form_class=ExpensesForm
    template_name='investment&expenses/expenses_update.html'
    success_url=reverse_lazy('investmentsandexpenses:expenses')
    
    def form_valid(self, form):
        self.object = form.save(commit=False)
        self.object.save()
        return super().form_valid(form)

def get_monthly_expense():
   # Calculate total expenses for each month up to the current month
        monthly_expenses = []
        current_date=nepali_datetime.date.today()
        current_year = current_date.year
        current_month = current_date.month
        
        for month in range(1, current_month + 1):
            start_date = f"{current_year}-{month:02d}-01"
            end_date = f"{current_year}-{month:02d}-29"  # Assuming 31 days in a month
            # expenses_for_month=get_object_or_404(Expenses,)
            expenses_for_month = Expenses.objects.filter(date__range=[start_date, end_date])
            total_expenses_for_month = expenses_for_month.aggregate(total=Sum('amount'))['total'] or 0
            monthly_expenses.append({
                'month': month,
                'total_expenses': total_expenses_for_month,
                'expenses_list': expenses_for_month,
            })
        return monthly_expenses

def get_monthly_purchase():
   # Calculate total expenses for each month up to the current month
        monthly_purchase = []
        current_date=nepali_datetime.date.today()
        current_year = current_date.year
        current_month = current_date.month
        
        for month in range(1, current_month + 1):
            start_date = f"{current_year}-{month:02d}-01"
            end_date = f"{current_year}-{month:02d}-29"  # Assuming 31 days in a month
            purchase_for_month = Stock.objects.filter(Nepali_date__range=[start_date, end_date],status='completed')
            total_purchase_for_month = purchase_for_month.aggregate(total=Sum('Grand_total'))['total'] or 0
            total_goldpurchase_for_month = purchase_for_month.aggregate(total=Sum('Total_weight'))['total'] or 0
            monthly_purchase.append({
                'month': month,
                'total_purchase': total_purchase_for_month,
                'total_goldpurchase':total_goldpurchase_for_month,
                'purchase_list': purchase_for_month,
            })
        return monthly_purchase

    
def get_monthly_sales():
   # Calculate total sales for each month up to the current month
        monthly_sales = []
        current_date=nepali_datetime.date.today()
        current_year = current_date.year
        current_month = current_date.month
        
        for month in range(1, current_month + 1):
            start_date = f"{current_year}-{month:02d}-01"
            end_date = f"{current_year}-{month:02d}-29"  # Assuming 31 days in a month
            sales_for_month = Sales.objects.filter(nepali_date__range=[start_date, end_date],status='approved')
            total_sales_for_month = sales_for_month.aggregate(total=Sum('grand_total'))['total'] or 0
            total_goldsales_for_month = sales_for_month.aggregate(total=Sum('total_weight'))['total'] or 0
            monthly_sales.append({
                'month': month,
                'total_sales': total_sales_for_month,
                'total_goldsales_for_month':total_goldsales_for_month,
                'sales_list': sales_for_month,
            })
        return monthly_sales

def get_monthly_profit():
   # Calculate total sales for each month up to the current month
        monthly_profit = []
        current_date=nepali_datetime.date.today()
        current_year = current_date.year
        current_month = current_date.month
        
        for month in range(1, current_month + 1):
            start_date = f"{current_year}-{month:02d}-01"
            end_date = f"{current_year}-{month:02d}-29"  # Assuming 31 days in a month
            profit_for_month = Sales.objects.filter(nepali_date__range=[start_date, end_date],status='approved')
            total_profit_for_month = profit_for_month.aggregate(total=Sum('profit'))['total'] or 0
            monthly_profit.append({
                'month': month,
                'total_profit': total_profit_for_month,
                'profit_list': profit_for_month,
            })
        return monthly_profit
      
class SummaryListView(PermissionRequiredMixin,View):
    model=Expenses
    permission_required='superuserstatus'
    template_name = 'investment&expenses/summary.html'

    def get(self, request):
        form=ExpensesForm()
        expenses = Expenses.objects.all()
        sales= Sales.objects.filter(status='approved')
        purchase=Stock.objects.filter(status='completed')
        totalsales=sales.aggregate(total=Sum('grand_total')).get('total') or 0
        totalgoldsales=sales.aggregate(total=Sum('total_weight')).get('total') or 0
        totalexpenses=expenses.aggregate(total=Sum('amount')).get('total') or 0
        totalpurchase=purchase.aggregate(total=Sum('Grand_total')).get('total') or 0
        totalgoldpurchase=purchase.aggregate(total=Sum('Total_weight')).get('total') or 0
        totalprofit=sales.aggregate(total=Sum('profit')).get('total') or 0
        current_date=nepali_datetime.date.today()
        current_year = current_date.year
        current_month = current_date.month
        monthly_expenses=get_monthly_expense()
        monthly_sales=get_monthly_sales()
        monthly_purchase=get_monthly_purchase()
        monthly_profit=get_monthly_profit()
        puregold=Stock.objects.filter(status='approved').exclude(Purchased_from='Customer')
        pure_gold=puregold.aggregate(total_weight_sum=Sum('Total_weight')).get('total_weight_sum') or 0
        stock_gold=pure_gold
        stock_value=(stock_gold/11.664)*110000
        bank_debit = Transaction.objects.filter(transaction_choices='Debit',status='approved').aggregate(total=Sum('bank'))['total'] or 0
        bank_credit = Transaction.objects.filter(transaction_choices='Credit',status='approved').aggregate(total=Sum('bank'))['total'] or 0
        bank_total=bank_credit-bank_debit
        cash_debit=Transaction.objects.filter(transaction_choices='Debit',status='approved').aggregate(total=Sum('cash'))['total'] or 0
        cash_credit=Transaction.objects.filter(transaction_choices='Credit',status='approved').aggregate(total=Sum('cash'))['total'] or 0
        cash_total=cash_credit-cash_debit

        due_debit=Transaction.objects.filter(transaction_choices='Debit',status='approved').aggregate(total=Sum('due'))['total'] or 0
        due_credit=Transaction.objects.filter(transaction_choices='Credit',status='approved').aggregate(total=Sum('due'))['total'] or 0
        due_total=due_credit-due_debit
        
        ornaments=Ornaments.objects.filter(status='approved',stock='AVAILABLE')
        totalornaments=ornaments.aggregate(total_sum=Sum('grand_total')).get('total_sum') or 0
        
        investment=Investment.objects.all()
        totalinvestment=investment.aggregate(total_sum=Sum('amount')).get('total_sum') or 0
        
        
        kaligargold=Kaligarorder.objects.all()
        totalkaligargold=kaligargold.aggregate(total_sum=Sum('gold_diyeko')).get('total_sum') or 0
        kaligargold=float(totalkaligargold)
        kaligargold=(kaligargold/11.664)*112000
            
        all_total=float(stock_value)+float(bank_total)+float(cash_total)+float(due_total)+float(totalornaments)+float(totalinvestment)+float(kaligargold)
        
        capital = Capital.objects.all().order_by('-date')
        total_capital=capital.aggregate(total=Sum('amount')).get('total') or 0
        
        profit=(int(all_total)+totalexpenses)-total_capital
        context={
            'form':form,
            'expenses': expenses,
            'totalexpenses':totalexpenses,
            'totalsales':totalsales,
            'totalpurchase':totalpurchase,
            'totalgoldpurchase':totalgoldpurchase,
            'totalgoldsales':totalgoldsales,
            'totalprofit':totalprofit,
            'expenses': expenses,
            'current_year': current_year,
            'current_month': current_month,
            'monthly_expenses': monthly_expenses,
            'monthly_purchase':monthly_purchase,
            'monthly_profit':monthly_profit,
            'monthly_sales':monthly_sales,
            'stock_gold':stock_gold,
            'stock_value':stock_value,
            'bank_total':bank_total,
            'cash_total':cash_total,
            'due_total':due_total,
            'totalornaments':totalornaments,
            'kaligargold':kaligargold,
            'totalinvestment':totalinvestment,
            'all_total':all_total,
            'total_capital':total_capital,
            'profit':profit
        }
        return render(request, self.template_name, context)

    def post(self, request):
        form = ExpensesForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect('investmentsandexpenses:expenses')
        return render(request, self.template_name, {'form': form})

    def delete(self,request):
        id=json.loads(request.body)['id']
        expense=get_object_or_404(Expenses,id=id)
        expense.delete()
        return redirect('investmentandexpenses:expenses')
    
    def update(self, request, expense_id):
            expense = get_object_or_404(Expenses, id=expense_id)
            if request.method == 'POST':
                form = ExpensesForm(request.POST, instance=expense)
                if form.is_valid():
                    form.save()
                    return redirect('investmentsandexpenses:expenses') 
            else:
                form = ExpensesForm(instance=expense)
            return render(request, self.template_name, {'form': form})