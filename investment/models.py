from django.db import models
from nepali_datetime_field.models import NepaliDateField

class Investment(models.Model):
    CATEGORY_CHOICES = (
        ('Stock', 'Stock'),
        ('Silver', 'Silver'),
        ('Golddeposit', 'Golddeposit'),
        ('Finance', 'Finance'),
        ('Savings', 'Savings'),
        ('Insurance', 'Insurance'),
        ('Other', 'Other'),
    )
    
    INVESTMENT_CHOICES = (
        ('Personel', 'Personel'),
        ('Shop', 'Shop'),
        ('Other', 'Other'),
    )

    STATUS_CHOICES=(
        ('approved','Approved'),
        ('completed','Completed'),
        ('deleted','Deleted'),
    )

    investment_type=models.CharField(blank=True,default='',max_length=100, choices=INVESTMENT_CHOICES)
    category = models.CharField(blank=True,default='',max_length=100, choices=CATEGORY_CHOICES)
    name = models.CharField(max_length=100)
    amount = models.DecimalField(max_digits=10, decimal_places=2)
    date=NepaliDateField()
    description = models.TextField(blank=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    interest=models.DecimalField(max_digits=10, default=0, blank=True,decimal_places=2)
    status=models.CharField(max_length=50,choices=STATUS_CHOICES,null=True, blank=True)
    
    def __str__(self):
        return self.name

class Expenses(models.Model):
    CATEGORY_CHOICES = (
        ('Salary', 'Salary'),
        ('Rent', 'Rent'),
        ('Utilities', 'Utilities'),
        ('Supplies', 'Supplies'),
        ('Marketing', 'Marketing'),
        ('Interest', 'Interest'),
        ('Insurance', 'Insurance'),
        ('Shop', 'Shop'),
        ('Other', 'Other'),
    )

    category = models.CharField(max_length=100, choices=CATEGORY_CHOICES)
    amount = models.DecimalField(max_digits=10, decimal_places=2)
    date = NepaliDateField()
    description = models.TextField(blank=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return f"{self.category} - {self.amount}"

class Capital(models.Model):
    name = models.CharField(max_length=100)
    description = models.TextField(blank=True)
    amount = models.DecimalField(max_digits=10, decimal_places=2)
    date = models.DateField()

    def __str__(self):
        return self.name