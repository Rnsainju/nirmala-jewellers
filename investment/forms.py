from django import forms
from .models import Investment, Expenses, Capital
from nepali_datetime_field.forms import NepaliDateField
import nepali_datetime

class InvestmentForm(forms.ModelForm):
    class Meta:
        model = Investment
        fields = ['investment_type','category','name', 'amount', 'date', 'description','interest']
        
        widgets={
            'investment_type':forms.Select(attrs={'class': 'form-control'}),
            'category':forms.Select(attrs={'class': 'form-control'}),
            'name':forms.TextInput(attrs={'class': 'form-control'}),
            'amount':forms.TextInput(attrs={'class': 'form-control'}),
            'date':forms.TextInput(attrs={'class': 'form-control'}),
            'description':forms.TextInput(attrs={'class': 'form-control'}),
            'interest':forms.TextInput(attrs={'class': 'form-control'})
        }
        date=NepaliDateField()
        
    def __init__(self,*args,**kwargs):
        super(InvestmentForm, self).__init__(*args, **kwargs)
        nepali_date=nepali_datetime.date.today()
        self.fields['date'].initial=nepali_date
        
        
class ExpensesForm(forms.ModelForm):
    class Meta:
        model = Expenses
        fields = ['category', 'amount', 'date', 'description']
        
        widgets={
            'category':forms.Select(attrs={'class': 'form-control'}),
            'amount':forms.TextInput(attrs={'class': 'form-control'}),
            'date':forms.TextInput(attrs={'class': 'form-control'}),
            'description':forms.TextInput(attrs={'class': 'form-control'})
        }
        date=NepaliDateField()
        
    def __init__(self,*args,**kwargs):
        super(ExpensesForm, self).__init__(*args, **kwargs)
        nepali_date=nepali_datetime.date.today()
        self.fields['date'].initial=nepali_date

class CapitalForm(forms.ModelForm):
    class Meta:
        model = Capital
        fields = ['name', 'amount', 'date', 'description']
        
        widgets={
            'name':forms.TextInput(attrs={'class': 'form-control'}),
            'amount':forms.TextInput(attrs={'class': 'form-control'}),
            'date':forms.TextInput(attrs={'class': 'form-control'}),
            'description':forms.TextInput(attrs={'class': 'form-control'}),
        }
        date=NepaliDateField()
        
    def __init__(self,*args,**kwargs):
        super(CapitalForm, self).__init__(*args, **kwargs)
        nepali_date=nepali_datetime.date.today()
        self.fields['date'].initial=nepali_date
        