from django import forms
from django.contrib.auth import get_user_model
from nepali_datetime_field.forms import NepaliDateField
from inventory.models import Ornaments
from .models import Sales, Order, SelectedOrnament
import nepali_datetime
from django_select2.forms import Select2MultipleWidget

User=get_user_model()

class SalesUploadForm(forms.Form):
    sales_file = forms.FileField(label='Upload Sales File', widget=forms.ClearableFileInput(attrs={'class': 'form-control-file'}))

    def clean_stock_file(self):
        sales_file = self.cleaned_data['sales_file']
        if sales_file:
            if not sales_file.name.endswith('.xlsx'):
                raise forms.ValidationError('Invalid file format. Please upload a valid Excel file.')
        return sales_file

class SearchOrnamentsWidget(Select2MultipleWidget):
    search_fields = [
        'product_name__icontains',
        'weight__icontains',
        'purity__icontains',
        'material__icontains',
    ]
      
          
class OrderForm(forms.ModelForm):
    selected_ornaments = forms.ModelMultipleChoiceField(
    queryset=SelectedOrnament.objects.filter(order_status='PROCESSING'),
    widget=SearchOrnamentsWidget(attrs={'style': 'width: 100%;'}),
    )

    class Meta:
        model = Order
        fields =['nepali_date','customer','address','phone_no','selected_ornaments','discounts','own_gold','cash','bank','due','notes']
        widgets = {
            'nepali_date': forms.DateInput(attrs={'class': 'form-control'}),
            'customer': forms.TextInput(attrs={'class': 'form-control','required': 'true'}),
            'address': forms.TextInput(attrs={'class': 'form-control'}),
            'phone_no': forms.TextInput(attrs={'class': 'form-control'}),
            'discounts': forms.TextInput(attrs={'class': 'form-control'}),
            'own_gold': forms.TextInput(attrs={'class': 'form-control'}),
            'cash': forms.TextInput(attrs={'class': 'form-control'}),
            'bank': forms.TextInput(attrs={'class': 'form-control'}),
            'due': forms.TextInput(attrs={'class': 'form-control'}),
            'notes': forms.Textarea(attrs={'class': 'form-control'}),
        }
        date:NepaliDateField()   


    def label_from_instance(self, obj):
            return f"{obj.product_name} - Weight: {obj.weight} - Purity: {obj.purity} - Material: {obj.material}"    
     
     
    def __init__(self,*args,**kwargs):
        super(OrderForm, self).__init__(*args, **kwargs)
        nepali_date=nepali_datetime.date.today()
        self.fields['nepali_date'].initial = nepali_date


class selectedOrnamentForm(forms.ModelForm):

    class Meta:
        model=SelectedOrnament
        fields=['ornament','rate','jarti','jyala','stone']
        widgets={
            'ornament': forms.Select(attrs={'class': 'form-control col-12'}),
            'rate': forms.TextInput(attrs={'class': 'form-control'}),
            'jarti': forms.TextInput(attrs={'class': 'form-control'}),
            'jyala': forms.TextInput(attrs={'class': 'form-control'}),
            'stone': forms.TextInput(attrs={'class': 'form-control'}),
        }
   
    def __init__(self, *args, **kwargs):
        super(selectedOrnamentForm, self).__init__(*args, **kwargs)
        
        # Modify the queryset for the 'ornament' field to only show available ornaments
        available_ornaments = Ornaments.objects.filter(stock='AVAILABLE').order_by('product_name')  # Adjust the filter criteria as needed
        self.fields['ornament'].queryset = available_ornaments
    
class SalesForm(forms.ModelForm):
    
    selected_ornaments = forms.ModelMultipleChoiceField(
    queryset=SelectedOrnament.objects.filter(order_status='PROCESSING'),
    widget=SearchOrnamentsWidget(attrs={'style': 'width: 100%;'}),
    )
    
    class Meta:
        model = Sales
        fields = ['bill_no','nepali_date','customer','address','phone_no','selected_ornaments','own_gold','discounts','total_weight','grand_total','profit','cash','bank','due','notes']
        widgets = {
            'bill_no': forms.TextInput(attrs={'class': 'form-control'}),
            'nepali_date': forms.DateInput(attrs={'class': 'form-control'}),
            'customer': forms.TextInput(attrs={'class': 'form-control'}),
            'address': forms.TextInput(attrs={'class': 'form-control'}),
            'phone_no': forms.TextInput(attrs={'class': 'form-control'}),
            'discounts': forms.TextInput(attrs={'class': 'form-control'}),
            'own_gold': forms.TextInput(attrs={'class': 'form-control'}),
            'cash': forms.TextInput(attrs={'class': 'form-control'}),
            'bank': forms.TextInput(attrs={'class': 'form-control'}),
            'due': forms.TextInput(attrs={'class': 'form-control'}),
            'notes': forms.Textarea(attrs={'class': 'form-control'}),
            'total_weight': forms.TextInput(attrs={'class': 'form-control'}),
            'grand_total': forms.TextInput(attrs={'class': 'form-control'}),
            'profit': forms.TextInput(attrs={'class': 'form-control'}),
        }
        date:NepaliDateField()
        
    def label_from_instance(self, obj):
            return f"{obj.product_name} - Weight: {obj.weight} - Purity: {obj.purity} - Material: {obj.material}"
        
    def __init__(self,*args,**kwargs):
        super(SalesForm, self).__init__(*args, **kwargs)
        nepali_date=nepali_datetime.date.today()
        self.fields['nepali_date'].initial = nepali_date

        


