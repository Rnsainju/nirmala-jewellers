from django.shortcuts import render, redirect, get_object_or_404
from .models import Sales, Order, SelectedOrnament
from inventory.models import Stock, Ornaments, PurchaseSource
from django.contrib.auth import authenticate,login,get_user_model
from django.views.generic import View, ListView, DetailView, TemplateView, UpdateView,DeleteView
from django.contrib.auth.mixins import PermissionRequiredMixin
import json
from django.urls import reverse_lazy
from django.db.models import Sum
from .forms import SalesForm, SalesUploadForm, OrderForm, selectedOrnamentForm
from inventory.forms import OrnamentForm
import openpyxl
from django.http import JsonResponse


class OrderView(View):
    model=Sales
    context_object_name = 'order'
    permission_required='superuserstatus'
    template_name='order/orders.html'

    def get(self, request):
        # order_upload_form = OrderUploadForm(request.POST, request.FILES)
        form = OrderForm()
        order_status=self.request.GET.get('order')
        if order_status:
            order= Order.objects.filter(order_status=order_status)
        else:
            order=Order.objects.filter(order_status='created')
        gold_order=order.aggregate(total_weight_sum=Sum('total_weight')).get('total_weight_sum') or 0
        total_order=order.aggregate(total=Sum('grand_total')).get('total') or 0
        context = {
            # 'sales_upload_form':sales_upload_form,
            'form': form,
            'order':order,
            'gold_order':gold_order,
            'total_order':total_order
            }
        return render(request, 'order/order.html', context)

    def delete(self,request):
        id=json.loads(request.body)['id']
        order=get_object_or_404(Order,id=id)
        order.order_status='deleted'
        order.save()
        return redirect('sales:order')


class OrderCreateView(View):
    model=Order
    context_object_name = 'order'
    permission_required='superuserstatus'
    template_name='order/order_create.html'

    def get(self, request):
        selected_ornaments=SelectedOrnament.objects.filter(order_status='PROCESSING')
        form=OrnamentForm()
        selectedornamentform=selectedOrnamentForm()
        orderform = OrderForm()
        return render(request, self.template_name, 
                      {
                        'orderform': orderform,
                        'form':form,
                        'selectedornamentform':selectedornamentform,
                        'selected_ornaments':selected_ornaments,
                        })

    def post(self,request):
        form=OrnamentForm(self.request.POST,self.request.FILES or None)
        selectedornamentform=selectedOrnamentForm(self.request.POST,self.request.FILES or None)
        orderform=OrderForm(self.request.POST,self.request.FILES or None)
        if orderform.is_valid():
            order=orderform.save(commit=False)
            order.status = 'approved'
            order.save()
            
            # Get the selected ornaments from the form
            selected_ornaments = orderform.cleaned_data['selected_ornaments']
            print(selected_ornaments)

            # # Get the selected ornaments from the form data (assuming the form has a field named 'ornaments')
            selected_ornaments_ids = request.POST.getlist('selected_ornaments')
            print(selected_ornaments)
            
            # # Update the status of selected ornaments to 'in-approved'
            ornaments_to_update = Ornaments.objects.filter(pk__in=selected_ornaments_ids)
            ornaments_to_update.update(stock='ORDERED')

            # Add the selected ornaments to the Sales object
            for ornament in selected_ornaments:
                order.selected_ornaments.add(ornament)

            # Calculate the total weight and save the object again
            order.calculate_total_weight()
            order.save()
            return redirect('sales:order')
        if form.is_valid():
            ornament=form.save(commit=False)
            ornament.status='approved'
            ornament.save()
            return redirect('sales:order-create')

        if selectedornamentform.is_valid():
            selected=selectedornamentform.save(commit=False)
            selected.save()
            print("success")
            return redirect('sales:order-create')
            
        return render(request, self.template_name, {'orderform': orderform})

class OrderUpdateView(PermissionRequiredMixin, View):
    permission_required = 'superuserstatus'
    model = Order
    template_name = 'order/order_update.html'

    def get(self, request, pk):
        order = get_object_or_404(Order, pk=pk)
        form = OrderForm(instance=order)
        return render(request, self.template_name, {'form': form, 'order': order})

    def post(self, request, pk):
        order = get_object_or_404(Order, pk=pk)
        form = OrderForm(self.request.POST, self.request.FILES, instance=order)
        if form.is_valid():
            updated_order = form.save(commit=False)
            updated_order.save()

            # Get the selected ornaments from the form
            selected_ornaments = form.cleaned_data['ornaments']
            selected_ornaments_ids = request.POST.getlist('ornaments')

            # Update the status of selected ornaments to 'ORDERED'
            ornaments_to_update = Ornaments.objects.filter(pk__in=selected_ornaments_ids)
            ornaments_to_update.update(stock='ORDERED')

            # Clear existing ornaments and add the selected ones
            updated_order.ornaments.clear()
            updated_order.ornaments.add(*selected_ornaments)

            # Calculate the total weight and save the object again
            updated_order.calculate_total_weight()
            updated_order.save()

            return redirect('sales:order')
        return render(request, self.template_name, {'form': form, 'order': order})

# def get_selectedornament_details(request, ornament_id):
#     # Retrieve the ornament based on the ornament_id
#     selected_ornament = get_object_or_404(SelectedOrnament, pk=ornament_id)

#     # Prepare the ornament details as a dictionary
#     ornament_details = {
#         'rate': selected_ornament.rate,
#         'wastage': selected_ornament.jarti,
#         'wages': selected_ornament.jyala,
#         'stones': selected_ornament.stone,
#     }

#     # Return the ornament details as JSON response
#     return JsonResponse(ornament_details)

def convert_orders_to_sales(request, order_id):
    # Get the order object based on the order_id
    order = get_object_or_404(Order, id=order_id)
    
    # Create a new Sales object using the order data
    sales = Sales.objects.create(
        bill_no=order.bill_no,
        nepali_date=order.nepali_date,
        customer=order.customer,
        address=order.address,
        phone_no=order.phone_no,
        total_weight=order.total_weight,
        own_gold=order.own_gold,
        discounts=order.discounts,
        grand_total=order.grand_total,
        cash=order.cash,
        bank=order.bank,
        due=order.due,
        notes=order.notes,
        status='approved'  # Assuming the initial status of the sales object is 'approved'
    )

    # Transfer the selected ornaments from the order to the sales
    for ornament in order.selected_ornaments.all():
        sales.selected_ornaments.add(ornament)
        
    # Save the new sales object
    sales.save()

    # Calculate the total weight and save the object again
    sales.calculate_total_weight()
    sales.save()
            
    # Calculate the profit and save the object again
    sales.calculate_profit()
    sales.save()


    # Mark the order as converted to sales
    order.order_status = 'completed'
    order.save()

    return redirect('sales:order')

class SalesView(View):
    model=Sales
    context_object_name = 'sales'
    permission_required='superuserstatus'
    template_name='sales/sales.html'

    def get(self, request):
        sales_upload_form = SalesUploadForm(request.POST, request.FILES)
        form = SalesForm()
        sales=Sales.objects.filter(status='approved')
        sales=sales.order_by('-bill_no')
        gold_sales=sales.aggregate(total_weight_sum=Sum('total_weight')).get('total_weight_sum') or 0
        total_sales=sales.aggregate(total=Sum('grand_total')).get('total') or 0
        profit=sales.aggregate(total_profit=Sum('profit')).get('total_profit') or 0
        
        context = {
            'sales_upload_form':sales_upload_form,
            'form': form,
            'sales':sales,
            'gold_sales':gold_sales,
            'total_sales':total_sales,
            'profit':profit
            }
        return render(request, 'sales/sales.html', context)

    def post(self, request,*args, **kwargs):
        sales=Sales.objects.filter(status='approved',material='Gold')
        sales_upload_form = SalesUploadForm(request.POST, request.FILES)

        if  sales_upload_form.is_valid():
            print("Success 0")
            excel_file = request.FILES['sales_file']
            if excel_file.name.endswith('.xlsx'):
                workbook = openpyxl.load_workbook(excel_file)
                sheet = workbook.active
                already_uploaded=[]
                for row in sheet.iter_rows(min_row=2, values_only=True):
                    bill_no = row[0]
                    nepali_date = row[1]
                    customer= row[2]
                    address=row[3]
                    phone_no=row[4]
                    ornaments=row[5]
                    material=row[6]
                    purity=row[7]
                    weight=row[8]
                    jarti=row[9]
                    total_weight=row[10]
                    rate=row[11]
                    own_gold=row[12]
                    jyala=row[13]
                    stone=row[14]
                    discounts=row[15]
                    grand_total=row[16]
                    cash=row[17]
                    bank=row[18]
                    due=row[19]
                    notes=row[20]
                    status='approved'

                    print('customer:',customer)
                    if row[0]:
                        if Sales.objects.filter(bill_no=bill_no,).exists():
                                already_uploaded.append(customer)
                        else:
                            Sales.objects.create(
                                bill_no = bill_no,
                                nepali_date = nepali_date,
                                customer= customer,
                                address=address,
                                phone_no=phone_no,
                                ornaments=ornaments,
                                material=material,
                                purity=purity,
                                weight=weight,
                                jarti=jarti,
                                total_weight=float(total_weight),
                                rate=rate,
                                own_gold=own_gold,
                                jyala=jyala,
                                stone=stone,
                                discounts=discounts,
                                grand_total=grand_total,
                                cash=cash,
                                bank=bank,
                                due=due,
                                notes=notes,
                                status=status
                                    )
                            print(f"Created sales object : {customer}, Ornaments: {ornaments}")

                if already_uploaded:
                    message = f"The following sales are already uploaded: {', '.join(already_uploaded)}"
                    print("The following sales are already uploaded")
                    return render(request, self.template_name, {'message': message,'sales':sales})


        return render(request, self.template_name,{
            'sales_upload_form': sales_upload_form,
            'sales':sales
        })

    def delete(self,request):
        id=json.loads(request.body)['id']
        sales=get_object_or_404(Sales,id=id)
        sales.status='deleted'
        sales.save()
        return redirect('sales:sales')


class SalesCreateView(View):
    model=Sales
    context_object_name = 'sales'
    permission_required='superuserstatus'
    template_name='sales/sales_create.html'

    def get(self, request):
        selected_ornaments=SelectedOrnament.objects.filter(order_status='PROCESSING')
        form=OrnamentForm()
        selectedornamentform=selectedOrnamentForm()
        salesform = SalesForm()
        return render(request, self.template_name, {
            'salesform': salesform,
            'form':form,
            'selectedornamentform':selectedornamentform,
            'selected_ornaments':selected_ornaments,
            })

    def post(self,request):
        selectedornamentform=selectedOrnamentForm(self.request.POST,self.request.FILES or None)
        salesform=SalesForm(self.request.POST,self.request.FILES or None)
        

        if selectedornamentform.is_valid():
            selected_ornaments = selectedornamentform.save(commit=False)
            selected_ornaments.save()
            return redirect('sales:sales-create')
            
        if salesform.is_valid():
            sales=salesform.save(commit=False)
            sales.status='approved'
            sales.save()

            # Get the selected ornaments from the form
            selected_ornaments = salesform.cleaned_data['selected_ornaments']

            
            # Add the selected ornaments to the Sales object
            for ornament in selected_ornaments:
                sales.selected_ornaments.add(ornament)

            # Calculate the total weight and save the object again
            sales.calculate_total_weight()
            sales.calculate_profit()
            sales.save()
            return redirect('sales:sales')
        
        return render(request, self.template_name, {
            'salesform': salesform,
        })


class SalesUpdateView(PermissionRequiredMixin, UpdateView):
    permission_required = 'superuserstatus'
    model = Sales
    form_class = SalesForm
    template_name = 'sales/sales_update.html'
    success_url = reverse_lazy('sales:sales')

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        # You can add any additional context data or modify the form here
        selected_ornaments = SelectedOrnament.objects.filter(order_status='SOLD')
        context['selectedornamentform'] = selectedOrnamentForm()  # Add your form
        context['selected_ornaments'] = selected_ornaments  # Add your queryset
        return context
    
    def form_valid(self, form):
        self.object = form.save(commit=False)
        self.object.save()
        return super().form_valid(form)
