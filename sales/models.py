from django.db import models
from nepali_datetime_field.models import NepaliDateField
from inventory.models import Ornaments
from transaction.models import Kaligar

class SelectedOrnament(models.Model):

    STATUS_CHOICES=(
    ('approved','Approved'),
    ('in-approved','In-approved'),
    ('deleted','Deleted'),
    )

    ORDER_STATUS_CHOICES = (
        ('ORDERED', 'ORDERED'),
        ('PROCESSING', 'PROCESSING'),
        ('SOLD', 'SOLD'),
        ('CANCELLED', 'CANCELLED'),
    )

    # order = models.ForeignKey(Order, on_delete=models.CASCADE)
    ornament = models.ForeignKey(Ornaments, on_delete=models.CASCADE)
    rate=models.IntegerField(null=True, blank=True)
    jarti=models.FloatField(null=True, blank=True)
    jyala=models.IntegerField(null=True, blank=True)
    stone=models.IntegerField(null=True, blank=True)
    totalweight=models.FloatField()
    subtotal = models.FloatField()  # This stores the subtotal for this specific ornament in the order
    # name = models.CharField(max_length=100)  # Add this field to store the name
    status= models.CharField(max_length=20, choices=STATUS_CHOICES, default='approved')
    order_status = models.CharField(max_length=20, choices=ORDER_STATUS_CHOICES, default='PROCESSING')

    def __str__(self):
        return f"Name: {self.ornament.product_name} - Weight: {self.ornament.weight} - Purity: {self.ornament.purity} - Rate : {int(self.rate)} - Jarti : {float(self.jarti)} - Jyala : {int(self.jyala)} - Stone : {int(self.stone)} - Total :{int(self.subtotal)}"

    def calculate_subtotal(self):
        self.totalweight=self.ornament.weight+self.jarti
        self.subtotal=(self.totalweight/11.664)*self.rate
        self.subtotal=self.subtotal+self.jyala+self.stone
        self.ornament.stock='ORDERED'

    def save(self, *args, **kwargs):
        self.calculate_subtotal()
        super(SelectedOrnament, self).save(*args, **kwargs)

class Order(models.Model):

    STATUS_CHOICES=(
    ('approved','Approved'),
    ('in-approved','In-approved'),
    ('deleted','Deleted'),
    )

    ORDER_STATUS_CHOICES = (
        ('created', 'Created'),
        ('processing', 'Processing'),
        ('completed', 'Completed'),
        ('cancelled', 'Cancelled'),
    )

    MATERIAL_CHOICES=(
        ('Gold','gold'),
        ('Silver','silver'),
        )

    PURITY_CHOICES=(
        ('24 CARAT','24-carat'),
        ('22 CARAT','22-carat'),
        ('18 CARAT','18-carat')
        )

    PAYMENT_CHOICES=(
        ('Cash','cash'),
        ('Transfer','transfer'),
        ('Card','card'),
        )

    nepali_date=NepaliDateField(null=True, blank=True, default=None)
    # delivery_date=NepaliDateField(null=True, blank=True, default='2080-05-25')
    selected_ornaments = models.ManyToManyField(SelectedOrnament)
    customer=models.CharField(max_length=50,null=True, blank=True)
    address=models.CharField(max_length=50,null=True, blank=True)
    phone_no=models.IntegerField(null=True, blank=True)
    total_weight=models.FloatField(null=True, blank=True,)
    own_gold=models.FloatField(null=True, blank=True)
    discounts=models.IntegerField(null=True, blank=True)
    grand_total=models.FloatField(null=True, blank=True)
    cash=models.FloatField(default=None,null=True, blank=True)
    bank=models.FloatField(default=None,null=True, blank=True)
    due=models.FloatField(null=True, blank=True, default=None)
    notes=models.TextField(null=True, blank=True,)
    order_status = models.CharField(max_length=20, choices=ORDER_STATUS_CHOICES, default='created')
    bill_no=models.IntegerField(null=True, blank=True)
    profit=models.FloatField(null=True, blank=True)

    def __str__(self):
        return f"Order #{self.pk} - {self.customer}"

    def calculate_total_weight(self):
        grand_total = 0.0
        total_weight=0.0
        own_gold = self.own_gold or 0.0
        discounts = self.discounts or 0.0

        # Iterate through the selected ornaments and calculate the total weight
        for ornament in self.selected_ornaments.all():
            total_weight += ornament.totalweight
            grand_total += ornament.subtotal
            print(total_weight)

        self.total_weight=total_weight
        self.grand_total=grand_total-own_gold-discounts

        self.due=self.grand_total-(self.cash or 0) -(self.bank or 0)
        self.save()
        self.selected_ornaments.order_status='ORDERED'

    def calculate_profit(self):
        cost_price = 0.0

        # Iterate through the selected ornaments and calculate the cost price
        for ornament in self.selected_ornaments.all():
            cost_price += ornament.cost_price

        profit = self.grand_total - cost_price
        self.profit = profit
        self.save()

class Sales(models.Model):

    STATUS_CHOICES=(
    ('approved','Approved'),
    ('in-approved','In-approved'),
    ('deleted','Deleted'),
    )

    MATERIAL_CHOICES=(
        ('Gold','gold'),
        ('Silver','silver'),
        )

    PURITY_CHOICES=(
        ('24 CARAT','24-carat'),
        ('22 CARAT','22-carat'),
        ('18 CARAT','18-carat')
        )

    PAYMENT_CHOICES=(
        ('Cash','cash'),
        ('Transfer','transfer'),
        ('Card','card'),
        )


    bill_no=models.IntegerField(null=True, blank=True)
    nepali_date=NepaliDateField(null=True, blank=True, default=None)
    selected_ornaments = models.ManyToManyField(SelectedOrnament, blank=True)
    customer=models.CharField(max_length=50,null=True, blank=True)
    address=models.CharField(max_length=50,null=True, blank=True)
    phone_no=models.IntegerField(null=True, blank=True)
    total_weight=models.FloatField(null=True, blank=True,)
    own_gold=models.FloatField(null=True, blank=True)
    discounts=models.IntegerField(null=True, blank=True)
    grand_total=models.FloatField(null=True, blank=True)
    cash=models.FloatField(default=None,null=True, blank=True)
    bank=models.FloatField(default=None,null=True, blank=True)
    due=models.FloatField(null=True, blank=True, default=None)
    notes=models.TextField(null=True, blank=True,)
    status=models.CharField(max_length=50,choices=STATUS_CHOICES,null=True, blank=True)
    profit=models.FloatField(default=None,null=True, blank=True)

    def __str__(self):
        return f"Sales #{self.pk} - {self.customer}"

    def calculate_total_weight(self):
        grand_total = 0.0
        total_weight=0.0
        discounts = self.discounts or 0.0

        # Iterate through the selected ornaments and calculate the total weight
        for ornament in self.selected_ornaments.all():
            total_weight += ornament.totalweight
            grand_total += ornament.subtotal

        # Update the status of selected ornaments to 'SOLD'
        for selectedornament in self.selected_ornaments.all():
            selectedornament.order_status = 'SOLD'
            selectedornament.save()
            ornament_id=selectedornament.ornament.id
            ornaments_to_update=Ornaments.objects.filter(pk=ornament_id)
            ornaments_to_update.update(stock='SOLD')


        self.total_weight=total_weight
        self.grand_total=grand_total-discounts
        total=self.grand_total
        self.due=total-(self.own_gold or 0)-(self.cash or 0) -(self.bank or 0)
        self.save()

    def calculate_profit(self):
        cost_price=0.0
        # Iterate through the selected ornaments and calculate the total weight
        for selectedornament in self.selected_ornaments.all():
            cost_price+=selectedornament.ornament.grand_total

        profit=self.grand_total -cost_price
        self.profit=profit
        self.save()

