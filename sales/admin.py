from django.contrib import admin
from .models import Sales,Order,SelectedOrnament

admin.site.register(Sales)
admin.site.register(Order)
admin.site.register(SelectedOrnament)