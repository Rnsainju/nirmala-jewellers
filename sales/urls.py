from django.urls import include, path

from .views import *
from .views import convert_orders_to_sales
app_name ='sales'

urlpatterns = [
    path('orders/',OrderView.as_view(),name='order'),
    path('order/create',OrderCreateView.as_view(),name='search_ornaments'),
    path('order/create',OrderCreateView.as_view(),name='order-create'),
    path('order_update/<int:pk>/',OrderUpdateView.as_view(), name='order_update'),
    path('sales/',SalesView.as_view(),name='sales'),
    path('sales/create',SalesCreateView.as_view(),name='sales-create'),
    path('sales_update/<int:pk>/',SalesUpdateView.as_view(), name='sales_update'),
    path('convert_order_to_sales/<int:order_id>/', convert_orders_to_sales, name='convert_order_to_sales'),
    # path('deleted/', StockDeletedListView.as_view(), name='sales_deleted'),
    # path('restore/<int:pk>/', restore_stock, name='restore_sales'),

]
