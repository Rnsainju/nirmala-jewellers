from django import forms
from django.contrib.auth import get_user_model
from .models import Transaction, Kaligarorder
from nepali_datetime_field.forms import NepaliDateField

User=get_user_model()

class TransactionForm(forms.ModelForm):
    class Meta:
        model = Transaction
        fields = ['transaction_choices', 'customer','cash','bank','due','description']
        widgets = {
            'transaction_choices': forms.Select(attrs={'class': 'form-control'}),
            'customer':forms.TextInput(attrs={'class': 'form-control'}),
            'cash': forms.TextInput(attrs={'class': 'form-control'}),
            'bank': forms.TextInput(attrs={'class': 'form-control'}),
            'due': forms.TextInput(attrs={'class': 'form-control'}),
            'description':forms.TextInput(attrs={'class': 'form-control'}),
        }
        nepali_date:NepaliDateField()

class KaligarOrderForm(forms.ModelForm):
    class Meta:
        model = Kaligarorder
        fields = ['kaligar', 'gold_diyeko','description']
        widgets = {
            'kaligar': forms.Select(attrs={'class': 'form-control'}),
            'gold_diyeko':forms.TextInput(attrs={'class': 'form-control'}),
            'description':forms.TextInput(attrs={'class': 'form-control'}),
        }
        nepali_date:NepaliDateField()