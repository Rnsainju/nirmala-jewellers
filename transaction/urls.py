from django.urls import include, path

from .views import *

app_name ='transaction'

urlpatterns = [
    path('transactions/',TransactionListView.as_view(),name='transaction'),
    path('kaligarornaments/<int:pk>/',KaligarornamentsListView.as_view(),name='kaligarornaments'),
    path('transactions/kaligarorder',KaligarorderListView.as_view(),name='kaligarorder'),
    # path('stocks/create',StockCreateView.as_view(),name='stock-create'),
    path('transaction_update/<int:pk>/',TransactionUpdateView.as_view(), name='transaction_update'),
    # path('deleted/', StockDeletedListView.as_view(), name='stocks_deleted'),
    # path('stock-restore/<int:pk>/', restore_stock, name='restore_stock'),,
]
