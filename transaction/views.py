from django.shortcuts import render, redirect, get_object_or_404
from django.db.models import Sum
from .models import Transaction, Kaligar, Kaligarorder
from inventory.models import Ornaments
from django.views.generic import View, ListView, DetailView, TemplateView, UpdateView,DeleteView
from django.contrib.auth.mixins import PermissionRequiredMixin
from .forms import TransactionForm, KaligarOrderForm
import nepali_datetime
import json
from django.urls import reverse_lazy

class  TransactionListView(PermissionRequiredMixin,TemplateView):
    model=Transaction
    context_object_name = 'transaction'
    permission_required='superuserstatus'
    template_name='transactions/create.html'
    
    def get(self,request, *args, **kwargs):
        transaction_form=TransactionForm()
        transactions=Transaction.objects.filter(status='approved').order_by('-nepali_date')
        
        bank_debit = Transaction.objects.filter(transaction_choices='Debit',status='approved').aggregate(total=Sum('bank'))['total'] or 0
        bank_credit = Transaction.objects.filter(transaction_choices='Credit',status='approved').aggregate(total=Sum('bank'))['total'] or 0
        bank_total=bank_credit-bank_debit
        cash_debit=Transaction.objects.filter(transaction_choices='Debit',status='approved').aggregate(total=Sum('cash'))['total'] or 0
        cash_credit=Transaction.objects.filter(transaction_choices='Credit',status='approved').aggregate(total=Sum('cash'))['total'] or 0
        cash_total=cash_credit-cash_debit
        due_debit=Transaction.objects.filter(transaction_choices='Debit',status='approved').aggregate(total=Sum('due'))['total'] or 0
        due_credit=Transaction.objects.filter(transaction_choices='Credit',status='approved').aggregate(total=Sum('due'))['total'] or 0
        due_total=due_credit-due_debit
        print(due_total)
        queryset={
            'transaction_form':transaction_form,
            'transactions':transactions,
            'bank_total':bank_total,
            'cash_total':cash_total,
            'due_total':due_total,
            }
        return render(request,self.template_name,queryset)

    def post(self,request):
            transaction_form=TransactionForm(self.request.POST,self.request.FILES or None)
            transactions=Transaction.objects.all()
            queryset={
                    'transaction_form':transaction_form,
                    'transactions':transactions
            }
            
            if transaction_form.is_valid():
                transaction = transaction_form.save(commit=False)
                transaction.nepali_date=nepali_datetime.date.today()
                transaction.status='approved'
                transaction.save()
            return redirect('transaction:transaction')
                

    def delete(self,request):
        id=json.loads(request.body)['id']
        print(id)
        transaction=get_object_or_404(Transaction,id=id)
        transaction.status='deleted'
        transaction.save()
        return redirect('transaction:transaction')

class  KaligarornamentsListView(PermissionRequiredMixin,TemplateView):
    model=Kaligarorder
    context_object_name = 'kaligarorder'
    permission_required='superuserstatus'
    template_name='kaligar/kaligarornaments.html'

    def get(self,request, *args, **kwargs):
        ornaments=Ornaments.objects.filter(kaligar=self.kwargs['pk'],status='approved').order_by('-nepali_date')
        count_ornament=ornaments.count()
        total_gold=ornaments.aggregate(total=Sum('weight')).get('total') or 0
        total_jarti=ornaments.aggregate(total=Sum('jarti')).get('total') or 0
        total=total_gold+total_jarti
        
        queryset={
            'ornaments':ornaments,
            'count_ornament':count_ornament,
            'total_gold':total_gold,
            'total_jarti':total_jarti,
            'total':total,
            }
        return render(request,self.template_name,queryset)

    
class  KaligarorderListView(PermissionRequiredMixin,TemplateView):
    model=Kaligarorder
    context_object_name = 'kaligarorder'
    permission_required='superuserstatus'
    template_name='kaligar/kaligarorder.html'
    
    def get(self,request, *args, **kwargs):
        kaligars=Kaligar.objects.all()
        for kaligar in kaligars:
            ornaments=Ornaments.objects.filter(kaligar=kaligar.id,status='approved').order_by('-nepali_date')
            total_gold=ornaments.aggregate(total=Sum('weight')).get('total') or 0
            total_jarti=ornaments.aggregate(total=Sum('jarti')).get('total') or 0
            total=total_gold+total_jarti
            kaligar_orders = Kaligarorder.objects.filter(kaligar=kaligar.id,status='approved')
            gold_diyeko = sum(order.gold_diyeko for order in kaligar_orders)
            gold=float(total)-float(gold_diyeko)
            kaligarorder=Kaligar.objects.get(id=kaligar.id)
            kaligarorder.gold=gold
            kaligarorder.save()
        kaligarorder_form=KaligarOrderForm()
        kaligarorder=Kaligarorder.objects.filter(status='approved').order_by('-nepali_date')
        queryset={
            'kaligars':kaligars,
            'kaligarorder_form':kaligarorder_form,
            'kaligarorder':kaligarorder,
            }
        return render(request,self.template_name,queryset)

    def post(self,request):
            kaligarorder_form=KaligarOrderForm(self.request.POST,self.request.FILES or None)
            kaligarorder=Kaligarorder.objects.filter(status='approved').order_by('-nepali_date')
            queryset={
                    'kaligarorder_form':kaligarorder_form,
                    'kaligarorder':kaligarorder
            }
            
            if kaligarorder_form.is_valid():
                kaligarorder = kaligarorder_form.save(commit=False)
                kaligarorder.nepali_date=nepali_datetime.date.today()
                kaligarorder.status='approved'
                kaligarorder.save()
            return redirect('transaction:kaligarorder')
                

    def delete(self,request):
        id=json.loads(request.body)['id']
        print(id)
        kaligarorder=get_object_or_404(Kaligarorder,id=id)
        kaligarorder.status='deleted'
        kaligarorder.save()
        return redirect('transaction:kaligarorder')
    
class TransactionUpdateView(PermissionRequiredMixin, UpdateView):
    permission_required = 'superuserstatus'
    model = Transaction
    form_class = TransactionForm
    template_name = 'transactions/transaction_update.html'
    success_url = reverse_lazy('transaction:transaction')
    
    def form_valid(self, form):
        self.object = form.save(commit=False)
        self.object.save()
        return super().form_valid(form)