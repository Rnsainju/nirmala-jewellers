from django.db import models
from nepali_datetime_field.models import NepaliDateField
from django.db.models.signals import post_save
from django.dispatch import receiver

# Create your models here.
class Transaction(models.Model):

    TRANSACTION_CHOICES = (
        ('Debit', 'Debit'),
        ('Credit', 'Credit')
    )

    STATUS_CHOICES=(
    ('approved','Approved'),
    ('in-approved','In-approved'),
    ('deleted','Deleted'),
    )
    
    transaction_choices=models.CharField(max_length=10, choices=TRANSACTION_CHOICES)
    customer=models.CharField(max_length=10)
    cash = models.DecimalField(max_digits=10, decimal_places=2,blank=True, default=0)
    bank = models.DecimalField(max_digits=10, decimal_places=2,blank=True, default=0)
    due = models.DecimalField(max_digits=10, decimal_places=2,blank=True, default=0)
    nepali_date=NepaliDateField()
    description=models.TextField()
    status=models.CharField(max_length=50,choices=STATUS_CHOICES,default="approved")

    def __str__(self):
        return f"{self.customer}"

            
class Kaligar(models.Model):
    name=models.CharField(max_length=10)
    gold=models.DecimalField(max_digits=10, decimal_places=3)
    
    def __str__(self):
        return f"{self.name}"

    
class Kaligarorder(models.Model):
     
    TRANSACTION_CHOICES = (
        ('Debit', 'Debit'),
        ('Credit', 'Credit')
    )

    STATUS_CHOICES=(
    ('approved','Approved'),
    ('in-approved','In-approved'),
    ('deleted','Deleted'),
    )
  
    nepali_date=NepaliDateField()
    kaligar=models.ForeignKey(Kaligar,on_delete=models.CASCADE)
    gold_diyeko=models.DecimalField(max_digits=10, decimal_places=3)
    cash=models.IntegerField(blank=True, default=0)
    description=models.TextField()
    status=models.CharField(max_length=50,choices=STATUS_CHOICES,default="approved")

    def __str__(self):
        return f"{self.kaligar.name}"
