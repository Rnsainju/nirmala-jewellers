from django.db.models.signals import post_save
from django.dispatch import receiver
from .models import Stock, Transaction


@receiver(post_save, sender=Stock)
def create_transaction(sender, instance, created, **kwargs):
    if created and instance.Purchased_from in ['Cash', 'Bank']:
        transaction_choices = 'Debit' if instance.Purchased_from == 'Cash' else 'Credit'
        cash = instance.Grand_total if instance.Purchased_from == 'Cash' else 0
        bank = instance.Grand_total if instance.Purchased_from == 'Bank' else 0
        description = f"Purchase of {instance.Material} ({instance.Purity}) - {instance.Total_weight} g"
        print("Signals running")
            # Create the new transaction entry
        Transaction.objects.create(
            transaction_choices=transaction_choices,
            customer="Supplier",  # Update with the appropriate supplier/customer name
            cash=cash,
            bank=bank,
            due=instance.Grand_total - (cash + bank),
            nepali_date=instance.Nepali_date,
            description=description,
            status='approved'  # Update with the appropriate status
        )
          