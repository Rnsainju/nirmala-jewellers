from django.apps import AppConfig


class TransactionConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'transaction'

def ready(self):
        # Import the signals module and connect the signal handlers
        import transaction.signals
