from django.contrib import admin

from .models import Transaction, Kaligar, Kaligarorder

admin.site.register(Transaction)
admin.site.register(Kaligar)
admin.site.register(Kaligarorder)