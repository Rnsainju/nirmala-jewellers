from django.urls import include, path

from .views import *

app_name ='stocks'

urlpatterns = [
    path('login/',login_page,name='login'),
    path('logout/',logout_view,name='logout'),
    path('register',register_page),
    path('',AdminHomeView.as_view(), name='home'),
    
    # Purchase source CRUD
    path('purchasesource/',PurchasesourceListView.as_view(),name='purchasesource'),
    path('purchasesourcedetail/',PurchasesourceDetailView.as_view(),name='purchasesourcedetail'),
    path('purchase-source_update/<int:pk>/',PurchaseSourceUpdateView.as_view(), name='purchase-source_update'),
    path('purchasesource-deleted/', PurchaseSourceDeletedListView.as_view(), name='purchasesource_deleted'),
    path('purchasesource-restore/<int:pk>/', restore_purchasesource, name='restore_purchasesource'),
    
    
    path('stocks/',GoldStockListView.as_view(),name='goldstocks'),
    path('stocks/create',StockCreateView.as_view(),name='stock-create'),
    path('stock_update/<int:pk>/',StockUpdateView.as_view(), name='stock_update'),
    path('deleted/', StockDeletedListView.as_view(), name='stocks_deleted'),
    path('stock-restore/<int:pk>/', restore_stock, name='restore_stock'),
    
    path('stockornament/',GoldOrnamentListView.as_view(),name='ornamentstock'),
    path('stockornaments/create',StockOrnamentCreateView.as_view(),name='stockornament-create'),
    
    path('ornaments-count/',OrnamentCountView.as_view(),name='ornamentscount'),
    path('ornaments/',OrnamentListView.as_view(),name='ornaments'),
    path('ornaments/create',OrnamentCreateView.as_view(),name='ornament-create'),
    path('sales-ornaments/create',SalesOrnamentCreateView.as_view(),name='sales-ornament-create'),
    path('ornament_update/<int:pk>/',OrnamentUpdateView.as_view(), name='ornament_update'),
    path('ornamentdeleted/', OrnamentDeletedListView.as_view(), name='ornaments_deleted'),
    path('ornament-restore/<int:pk>/', restore_ornament, name='restore_ornament'),
]
