from django import forms
from django.contrib.auth import get_user_model
from .models import Stock,StockOrnaments, Ornaments, PurchaseSource
from nepali_datetime_field.forms import NepaliDateField
# from nepali_date.widgets import NepaliDateField
import nepali_datetime
from django_select2.forms import Select2MultipleWidget

User=get_user_model()

class PurchaseUploadForm(forms.Form):
    purchase_file = forms.FileField(label='Upload Purchase Source File', widget=forms.ClearableFileInput(attrs={'class': 'form-control-file'}))

    def clean_purchase_file(self):
        purchase_file = self.cleaned_data['purchase_file']
        if purchase_file:
            if not purchase_file.name.endswith('.xlsx'):
                raise forms.ValidationError('Invalid file format. Please upload a valid Excel file.')
        return purchase_file


class StockForm(forms.ModelForm):
    class Meta:
        model = Stock
        fields = ['Material', 'Purity', 'Nepali_date','Purchased_from', 'Purchase_source','Total_weight', 'Rate', 'Grand_total','Cash','Bank','Due','Bill_no', 'Addition_gold_rate', 'Notes']

        widgets = {
            'Material': forms.Select(attrs={'class': 'form-control'}),
            'Purity': forms.Select(attrs={'class': 'form-control'}),
            'Nepali_date': forms.TextInput(attrs={'class': 'form-control'}),
            'Purchased_from': forms.Select(attrs={'class': 'form-control'}),
            'Purchase_source': forms.Select(attrs={'class': 'form-control'}),
            'Total_weight': forms.TextInput(attrs={'class': 'form-control'}),
            'Rate': forms.TextInput(attrs={'class': 'form-control'}),
            'Grand_total': forms.TextInput(attrs={'class': 'form-control'}),
            'Cash': forms.TextInput(attrs={'class': 'form-control'}),
            'Bank': forms.TextInput(attrs={'class': 'form-control'}),
            'Due': forms.TextInput(attrs={'class': 'form-control'}),
            'Bill_no': forms.TextInput(attrs={'class': 'form-control'}),
            'Addition_gold_rate': forms.TextInput(attrs={'class': 'form-control'}),
            'Notes': forms.TextInput(attrs={'class': 'form-control'}),
        }
        Nepali_date:NepaliDateField()

    def __init__(self,*args,**kwargs):
        super(StockForm, self).__init__(*args, **kwargs)
        self.fields['Material'].initial = 'Gold'
        self.fields['Purity'].initial = '24 CARAT'
        nepali_date=nepali_datetime.date.today()
        self.fields['Nepali_date'].initial = nepali_date
        self.fields['Purchase_source'].choices = [('', 'Select Purchase Source')] + list(self.fields['Purchase_source'].choices)

class SearchOrnamentsWidget(Select2MultipleWidget):
    search_fields = [
        'product_name__icontains',
        'weight__icontains',
        'purity__icontains',
        'material__icontains',
    ]
    
class StockOrnamentForm(forms.ModelForm):
    
    selected_ornaments = forms.ModelMultipleChoiceField(
        queryset=Ornaments.objects.filter(status='approved'),
        widget=SearchOrnamentsWidget(attrs={'style': 'width: 100%;'}),
    )

    class Meta:
        model=StockOrnaments
        fields=['bill_no','nepali_date','selected_ornaments','purchase_source','total_weight','own_gold','discounts','grand_total','cash','bank','due','notes']

        widgets={
            'bill_no': forms.TextInput(attrs={'class': 'form-control'}),
            'nepali_date': forms.DateInput(attrs={'class': 'form-control'}),
            'purchase_source': forms.Select(attrs={'class': 'form-control'}),
            'discounts': forms.TextInput(attrs={'class': 'form-control'}),
            'own_gold': forms.TextInput(attrs={'class': 'form-control'}),
            'cash': forms.TextInput(attrs={'class': 'form-control'}),
            'bank': forms.TextInput(attrs={'class': 'form-control'}),
            'due': forms.TextInput(attrs={'class': 'form-control'}),
            'notes': forms.Textarea(attrs={'class': 'form-control'}),
            'total_weight': forms.TextInput(attrs={'class': 'form-control'}),
            'grand_total': forms.TextInput(attrs={'class': 'form-control'}),
        }
        nepali_date:NepaliDateField()
        
    def __init__(self,*args,**kwargs):
        super(StockOrnamentForm, self).__init__(*args, **kwargs)
        nepali_date=nepali_datetime.date.today()
        self.fields['nepali_date'].initial = nepali_date
         
class StockUploadForm(forms.Form):
    stock_file = forms.FileField(label='Upload Stock File', widget=forms.ClearableFileInput(attrs={'class': 'form-control-file'}))

    def clean_stock_file(self):
        stock_file = self.cleaned_data['stock_file']
        if stock_file:
            if not stock_file.name.endswith('.xlsx'):
                raise forms.ValidationError('Invalid file format. Please upload a valid Excel file.')
        return stock_file

class OrnamentUploadForm(forms.Form):
    ornament_file = forms.FileField(label='Upload Ornament File', widget=forms.ClearableFileInput(attrs={'class': 'form-control-file'}))

    def clean_stock_file(self):
        ornament_file = self.cleaned_data['ornament_file']
        if ornament_file:
            if not ornament_file.name.endswith('.xlsx'):
                raise forms.ValidationError('Invalid file format. Please upload a valid Excel file.')
        return ornament_file
      
class PurchaseSourceForm(forms.ModelForm):
    class Meta:
        model = PurchaseSource
        fields = ['name', 'pan']
        widgets = {
            'name': forms.TextInput(attrs={'class': 'form-control'}),
            'pan': forms.NumberInput(attrs={'class': 'form-control'}),
        }

class OrnamentForm(forms.ModelForm):

    class Meta:
        model = Ornaments
        fields = ['nepali_date','material', 'purity', 'main_category','product_name','weight','jarti','rate','wages','stones','purchased_from','kaligar','cash','bank','due','stock']

        widgets = {
            'nepali_date': forms.TextInput(attrs={'class': 'form-control'}),
            'material': forms.Select(attrs={'class': 'form-control'}),
            'purity': forms.Select(attrs={'class': 'form-control'}),
            'main_category': forms.Select(attrs={'class': 'form-control'}),
            'product_name': forms.TextInput(attrs={'class': 'form-control','placeholder':'Enter the name of the product.'}),
            'weight': forms.TextInput(attrs={'class': 'form-control','placeholder':'Enter the weight of the ornament in grams.'}),
            'jarti': forms.TextInput(attrs={'class': 'form-control','placeholder':'Leave blank to calculate 5% automatically.'}),
            'rate': forms.TextInput(attrs={'class': 'form-control','placeholder':'Enter the rate in tola.'}),
            'wages': forms.TextInput(attrs={'class': 'form-control'}),
            'stones': forms.TextInput(attrs={'class': 'form-control'}),
            # 'grand_total': forms.TextInput(attrs={'class': 'form-control'}),
            'purchased_from': forms.Select(attrs={'class': 'form-control'}),
            'kaligar': forms.Select(attrs={'class': 'form-control'}),
            'cash': forms.TextInput(attrs={'class': 'form-control'}),
            'bank': forms.TextInput(attrs={'class': 'form-control'}),
            'due': forms.TextInput(attrs={'class': 'form-control'}),
            'stock': forms.Select(attrs={'class': 'form-control'}),
        }

        nepali_date:NepaliDateField()
        
    def __init__(self,*args,**kwargs):
        super(OrnamentForm, self).__init__(*args, **kwargs)
        self.fields['material'].initial = 'Gold'
        self.fields['purity'].initial = '24 CARAT'
        nepali_date=nepali_datetime.date.today()
        self.fields['nepali_date'].initial = nepali_date
        self.fields['product_name'].required = True
        self.fields['weight'].required = True
      
    def clean_jarti(self):
        jarti = self.cleaned_data.get('jarti')
        weight = self.cleaned_data.get('weight')

        # If jarti is not provided or is empty, calculate it from weight
        if not jarti:
            if weight:
                jarti = weight * 0.05  # Calculate jarti as 5% of weight
            else:
                raise forms.ValidationError("Jarti or Weight must be provided.")

        return jarti            
        
        
        
class LoginForm(forms.Form):
    username=forms.CharField(
        max_length=100,
        widget=forms.TextInput(attrs={
            'class':'input100',
            'placeholder':'User name',
        })
    )
    password=forms.CharField(
        widget=forms.PasswordInput(attrs={
            'class':'input100',
            'placeholder':'Password',
        })
        )

class RegisterForm(forms.Form):
    username=forms.CharField()
    email=forms.EmailField(
        widget=forms.EmailInput(
            attrs={
                "class":"form-control",
                "placeholder":"Your email"
            }
        )
    )
    password=forms.CharField(widget=forms.PasswordInput)
    password2=forms.CharField(label='Confirm password',widget=forms.PasswordInput)

    def clean_username(self):
        username=self.cleaned_data.get('username')
        qs=User.objects.filter(username=username)
        if qs.exists():
            raise forms.ValidationError("Username is taken")
        return username
    
    def clean_email(self):
        email=self.cleaned_data.get('email')
        qs=User.objects.filter(email=email)
        if qs.exists():
            raise forms.ValidationError("Email is taken")
        return email

    def clean(self):
        data=self.cleaned_data
        password =self.cleaned_data.get('password')
        password2=self.cleaned_data.get('password2')
        if password2 != password:
            raise forms.ValidationError("Password must match")
            return data