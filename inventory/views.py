from django.shortcuts import render, redirect, get_object_or_404
from .models import Stock, StockOrnaments,Ornaments, PurchaseSource, MainCategory, GoldRate
from sales.models import Sales, Order
from transaction.models import Transaction, Kaligar, Kaligarorder
from investment.models import Investment
from django.contrib.auth import authenticate,login,get_user_model
from django.views.generic import View, ListView, DetailView, TemplateView, UpdateView,DeleteView
from django.contrib.auth.mixins import PermissionRequiredMixin
from .forms import LoginForm,RegisterForm, StockForm, StockOrnamentForm,OrnamentForm, OrnamentUploadForm,PurchaseSourceForm, PurchaseUploadForm, StockUploadForm
import json
from django.urls import reverse_lazy
from django.db.models import Sum
import openpyxl
from django.db import transaction
from django.db.models import Count
from django.db.models import Q
from django.core.paginator import Paginator
import requests
from bs4 import BeautifulSoup
import nepali_datetime
from django.contrib.auth import logout

User =get_user_model()

class  PurchasesourceListView(PermissionRequiredMixin,TemplateView):
    model=PurchaseSource
    context_object_name = 'purchasesource'
    permission_required='superuserstatus'
    template_name='purchasesource.html'


    def get(self,request, *args, **kwargs):
        model_name= 'Purchase Source'
        form=PurchaseSourceForm()
        upload_form = PurchaseUploadForm(request.POST, request.FILES)
        purchase_source=PurchaseSource.objects.filter(status='approved')
        for item in purchase_source:
            id=item.pk
            list=Stock.objects.filter(status='completed',Purchase_source=id)
            print(list)
            total_weight=sum(item.Total_weight for item in list)
        total=0
        total=purchase_source
        queryset={'model_name':model_name,
                  'purchase_source':purchase_source,
                  'form':form,
                  'upload_form':upload_form,
                  'total_weight':total_weight,
                  'total':total,
                  }
        return render(request,self.template_name,queryset)

    def post(self,request):
            purchase_source=PurchaseSource.objects.all()
            purchase_form=PurchaseSourceForm(self.request.POST,self.request.FILES or None)
            upload_form = PurchaseUploadForm(request.POST, request.FILES)
            queryset={
                    'upload_form':upload_form,
                    'purchase_source':purchase_source,
                    'purchase_form':purchase_form
            }

            if purchase_form.is_valid():
                purchase = purchase_form.save(commit=False)
                purchase.save()
                return redirect('stocks:purchasesource')
            if upload_form.is_valid():
                excel_file = request.FILES['purchase_file']
                if excel_file.name.endswith('.xlsx'):
                    workbook = openpyxl.load_workbook(excel_file)
                    sheet = workbook.active
                    already_uploaded=[]
                    for row in sheet.iter_rows(min_row=2, values_only=True):
                        name = row[0]
                        pan = row[1]
                        if PurchaseSource.objects.filter(name=name).exists():
                            already_uploaded.append(name)
                        else:
                            PurchaseSource.objects.create(name=name, pan=pan)

                    if already_uploaded:
                        message = f"The following purchase sources are already uploaded: {', '.join(already_uploaded)}"
                        print("The following purchase sources are already uploaded")
                        return render(request, self.template_name,{
                            'upload_form':upload_form,
                            'purchase_source':purchase_source,
                            'purchase_form':purchase_form,
                            'message':message
                            })

            return render(request, self.template_name, queryset)

    def delete(self,request):
        id=json.loads(request.body)['id']
        stock=get_object_or_404(PurchaseSource,id=id)
        stock.status='deleted'
        stock.save()
        return redirect('stocks:purchasesource')

class PurchaseSourceUpdateView(PermissionRequiredMixin, UpdateView):
    permission_required = 'superuserstatus'
    model = PurchaseSource
    form_class = PurchaseSourceForm
    template_name = 'purchase-source_update.html'
    success_url = reverse_lazy('stocks:purchasesource')


    def form_valid(self, form):
        self.object = form.save(commit=False)
        self.object.save()
        return super().form_valid(form)

class PurchaseSourceDeletedListView(TemplateView):
    model=Stock
    context_object_name = 'deleted_purchasesource'
    permission_required='superuserstatus'
    template_name='purchasesource/deleted.html'

    def get(self, request, *args, **kwargs):
        deleted = PurchaseSource.objects.filter(status='deleted')
        queryset={'deleted':deleted}
        return render(request,self.template_name,queryset)

    def delete(self,request):
        id=json.loads(request.body)['id']
        stock=get_object_or_404(PurchaseSource,id=id)
        stock.delete()
        return redirect('stocks:purchasesource_deleted')

def restore_purchasesource(request, pk):
    item = PurchaseSource.objects.get(pk=pk)
    item.status = 'approved'
    item.save()
    return redirect('stocks:purchasesource_deleted')

def check_rate_update():
    todays_date=nepali_datetime.date.today()
    print(todays_date)
    gold_rate=gold_rate=get_object_or_404(GoldRate,gold_type="FINE GOLD (9999)", unit="10 grm")
    if (todays_date == gold_rate.nepalidate):
        print('successfuly updated')
    else:
        print('Not updated')
    # get_todays_rate()

def get_todays_rate():
    # Define the URL of the website you want to scrape
    url = "https://www.fenegosida.org/"

    # Send an HTTP GET request to the URL
    response = requests.get(url)

    # Check if the request was successful (status code 200)
    if response.status_code == 200:
        # Parse the HTML content using BeautifulSoup
        soup = BeautifulSoup(response.text, 'html.parser')

        # Find all div elements with the specified class
        day = soup.find('div', class_='rate-date-day')
        month=soup.find('div', class_='rate-date-month')
        year=soup.find('div', class_='rate-date-year')

           # Check if the div element was found
        if day and month and year:
            # Extract and print the text from the div element
            day = day.get_text()
            day=str(day.zfill(2))
            month = month.get_text()
            months={
                'Baisakh':'01',
                'Jestha':'02',
                'Ashad':'03',
                'Shrawan':'04',
                'Bhadra':'05',
                'Ashoj':'06',
                'Kartik':'07',
                'Mansir':'08',
                'Poush':'09',
                'Magh':'10',
                'Falgun':'11',
                'Chaitra':'12',
                }
            if month in months:
                month=months[month]
            else:
                # Handle the case where the month name is not found in the dictionary
                print(f"Month '{month}' not found in the dictionary.")
            year = year.get_text()
            date=f"{year}-{month}-{day}"
        else:
            print("Div element not found with the specified class.")

        goldrate=soup.find('div',class_='rate-gold post')

        rate=goldrate.get_text(strip=True)

        # Split the string by "per" to get type and the rest of the string
        type_part, rest = rate.split("per", 1)
        type_part = type_part.strip()

        # Split the rest by "Nrs" to get unit and price
        unit_part, price_part = rest.split("Nrs", 1)
        unit_part = unit_part.strip()
        price_part = price_part.strip("/-")

        # Output the variables
        print("Type:", type_part)
        print("Unit:", unit_part)
        print("Price:", price_part)
        print("Date:", date)

        try:
            gold_rate=get_object_or_404(GoldRate,gold_type="FINE GOLD (9999)", unit="10 grm")
        except GoldRate.DoesNotExist:
            gold_rate=None

        if gold_rate:
            # Update the price
            gold_rate.price = price_part
            gold_rate.nepalidate=date

            # Save the record
            gold_rate.save()
        else:
            # Handle the case where the record does not exist
            print("Record not found.")


        # silverrate=soup.find_all('div',class_='rate-silver post')

        # # Initialize variables
        # silver_type = ""
        # silver_unit = ""
        # silver_price = ""

        # # Iterate through the div elements
        # for div_element in silverrate:
        #     # Find and print text from p, span, and b tags within the div
        #     for tag in div_element.find_all(['p', 'span', 'b']):
        #         text = tag.get_text(strip=True)
        #         if "SILVER" in text:
        #             silver_type = text
        #             # Split the text based on "per" and take the first part
        #             split_text = silver_type.split("per")
        #             silver_type=split_text[0].strip()
        #         elif "per" in text:
        #             silver_unit = text
        #         else:
        #             silver_price = text

        #     # Print the extracted variables
        #     print("Silver Type:", silver_type)
        #     print("Unit:", silver_unit)
        #     print("Price:", silver_price)

    else:
        print("Failed to retrieve the webpage. Status code:", response.status_code)

def calculate_current_gold_stock():
    total_gold_stock = Stock.objects.filter(status='approved').aggregate(total_weight_sum=Sum('Total_weight')).get('total_weight_sum') or 0
    return total_gold_stock

def create_transaction(stock):
    if stock.Purchased_from in ['Cash', 'Bank']:
        transaction_choices = 'Debit' if stock.Purchased_from == 'Cash' else 'Credit'
        cash = stock.Grand_total if stock.Purchased_from == 'Cash' else 0
        bank = stock.Grand_total if stock.Purchased_from == 'Bank' else 0
        description = f"Purchase of {stock.Material} ({stock.Purity}) - {stock.Total_weight} g"
        print("Signals running")
            # Create the new transaction entry
        Transaction.objects.create(
            transaction_choices=transaction_choices,
            customer="Supplier",  # Update with the appropriate supplier/customer name
            cash=cash,
            bank=bank,
            due=stock.Grand_total - (cash + bank),
            nepali_date=stock.Nepali_date,
            description=description,
            status='approved'  # Update with the appropriate status
        )


class  AdminHomeView(PermissionRequiredMixin,TemplateView):
    permission_required='superuserstatus'
    template_name='index.html'

    def get(self,request, *args, **kwargs):
        # check_rate_update()
        username=request.user.username
        
        if request.user.is_superuser:
            usertype = 'SuperAdmin'
        else:
            usertype = 'Admin'
        
        print(username)
        puregold=Stock.objects.filter(status='approved').exclude(Purchased_from='Customer')
        goldsales=Sales.objects.filter(status='approved')

        pure_gold=puregold.aggregate(total_weight_sum=Sum('Total_weight')).get('total_weight_sum') or 0
        gold_sales=goldsales.aggregate(total_weight_sum=Sum('total_weight')).get('total_weight_sum') or 0
        sales=goldsales.aggregate(total_sales=Sum('grand_total')).get('total_sales_sum') or 0

        stock_gold=pure_gold
        stock_value=(stock_gold/11.664)*110000

        bank_debit = Transaction.objects.filter(transaction_choices='Debit',status='approved').aggregate(total=Sum('bank'))['total'] or 0
        bank_credit = Transaction.objects.filter(transaction_choices='Credit',status='approved').aggregate(total=Sum('bank'))['total'] or 0
        bank_total=bank_credit-bank_debit
        cash_debit=Transaction.objects.filter(transaction_choices='Debit',status='approved').aggregate(total=Sum('cash'))['total'] or 0
        cash_credit=Transaction.objects.filter(transaction_choices='Credit',status='approved').aggregate(total=Sum('cash'))['total'] or 0
        cash_total=cash_credit-cash_debit

        due_debit=Transaction.objects.filter(transaction_choices='Debit',status='approved').aggregate(total=Sum('due'))['total'] or 0
        due_credit=Transaction.objects.filter(transaction_choices='Credit',status='approved').aggregate(total=Sum('due'))['total'] or 0
        due_total=due_credit-due_debit

        ornaments=Ornaments.objects.filter(status='approved',stock='AVAILABLE')
        totalornaments=ornaments.aggregate(total_sum=Sum('grand_total')).get('total_sum') or 0

        investment=Investment.objects.all()
        totalinvestment=investment.aggregate(total_sum=Sum('amount')).get('total_sum') or 0
        
        kaligargold=Kaligarorder.objects.all()
        totalkaligargold=kaligargold.aggregate(total_sum=Sum('gold_diyeko')).get('total_sum') or 0
        kaligargold=float(totalkaligargold)
        kaligargold=(kaligargold/11.664)*112000
        
        transaction_total=float(bank_total)+float(cash_total)+float(due_total)
        investment_total=float(stock_value)+float(totalornaments)+float(totalinvestment)+float(kaligargold)

        order = Order.objects.filter(Q(order_status='created') | Q(order_status='processing'))

        gold_rate=get_object_or_404(GoldRate,gold_type="FINE GOLD (9999)", unit="10 grm")
        queryset={
            'username':username,
            'usertype': usertype,
            'stock_gold':stock_gold,
            'gold_sales':gold_sales,
            'bank_total':bank_total,
            'cash_total':cash_total,
            'due_total':due_total,
            'sales':sales,
            'order':order,
            'stock_value':stock_value,
            'totalornaments':totalornaments,
            'kaligargold':kaligargold,
            'totalinvestment':totalinvestment,
            'transaction_total':transaction_total,
            'investment_total':investment_total,
            'gold_rate':gold_rate
            }
        return render(request,self.template_name,queryset)

def login_page(request):
    if request.method == 'POST':
        form= LoginForm(request.POST)
        if form.is_valid():
            username= form.cleaned_data.get("username")
            password= form.cleaned_data.get("password")
            print(username)
            user =authenticate(request,username=username,password=password)
            if user is not None:
                print("User logged in")
                login(request,user)
                return redirect("stocks:home")
            else:
                print("Error")
    context={
        'form':LoginForm()
    }
    return render(request, "auth/login.html",context)

def logout_view(request):
    # Use the logout function to log the user out
    logout(request)
    
    # Redirect to a different page after logout (optional)
    return redirect('stocks:login')

def register_page(request):
    form=RegisterForm(request.POST or None)
    context={
        "form": form
    }
    if form.is_valid():
        print(form.cleaned_data)
        username= form.cleaned_data.get("username")
        email= form.cleaned_data.get("email")
        password= form.cleaned_data.get("password")
        new_user=User.objects.create_user(username,email,password)
        print(new_user)
    return render(request, "auth/register.html",context)



class  PurchasesourceDetailView(PermissionRequiredMixin,TemplateView):
    context_object_name = 'purchasesourcedetail'
    permission_required='superuserstatus'
    template_name='purchasesourcedetail.html'


    def get(self,request, *args, **kwargs):
        purchased_from=self.request.GET.get('purchased_from')
        if purchased_from:
            stocks=Stock.objects.filter(Purchase_source=purchased_from,status='completed')
            queryset=PurchaseSource.objects.get(id=purchased_from)
            purchasesource=queryset.name
            total=stocks.aggregate(total=Sum('Grand_total')).get('total') or 0
            totalweight=stocks.aggregate(total=Sum('Total_weight')).get('total') or 0
        else:
            print('No purchase made')
            redirect('stocks:purchasesource')
        queryset={'purchasesource':purchasesource,'stocks':stocks,'total':total,'totalweight':totalweight}
        return render(request,self.template_name,queryset)




class GoldStockListView(PermissionRequiredMixin,TemplateView):
    model=Stock
    context_object_name = 'stocks'
    permission_required='superuserstatus'
    template_name='stocks/stocks.html'

    def get(self,request, *args, **kwargs):
        stock_upload_form = StockUploadForm(request.POST, request.FILES)
        form=StockForm()
        purchased_from = self.request.GET.get('purchased_from')
        puregold=Stock.objects.filter(status='approved').exclude(Purchased_from='Customer')
        pure_gold=puregold.aggregate(total_weight_sum=Sum('Total_weight')).get('total_weight_sum') or 0
        mixgold=Stock.objects.filter(status='approved',Purchased_from='Customer')
        mix_gold=mixgold.aggregate(total_weight_sum=Sum('Total_weight')).get('total_weight_sum') or 0
        if purchased_from:
            stocks=Stock.objects.filter(status='completed',Purchased_from=purchased_from)
        else:
            stocks=Stock.objects.filter(status='completed').order_by('Nepali_date')
        totalweight=stocks.aggregate(total_weight_sum=Sum('Total_weight')).get('total_weight_sum') or 0
        total=stocks.aggregate(total_sum=Sum('Grand_total')).get('total_sum') or 0
        queryset={'stocks':stocks,
                  'stock_upload_form':stock_upload_form,
                  'form':form,
                  'totalweight':totalweight,
                  'pure_gold':pure_gold,
                  'mix_gold':mix_gold,
                  'total':total,
                  }
        return render(request,self.template_name,queryset)


    def post(self, request,*args, **kwargs):
        stocks=Stock.objects.filter(status='approved').order_by('Nepali_date')
        stock_upload_form = StockUploadForm(request.POST, request.FILES)

        if  stock_upload_form.is_valid():
            print("Success 0")
            excel_file = request.FILES['stock_file']
            if excel_file.name.endswith('.xlsx'):
                workbook = openpyxl.load_workbook(excel_file)
                sheet = workbook.active
                already_uploaded=[]
                for row in sheet.iter_rows(min_row=2, values_only=True):
                    print("Success 2")
                    Material = row[0]
                    Purity = row[1]
                    Nepali_date= row[2]
                    Purchased_from=row[3]
                    Purchase_source=row[4]
                    Total_weight=row[5]
                    Rate=row[6]
                    Grand_total=row[7]
                    Cash=row[8]
                    Bank=row[9]
                    Due=row[10]
                    Bill_no=row[11]
                    Addition_gold_rate=row[12]
                    Notes=row[13]

                    if row[0]:
                        if Purchase_source or None:
                            purchase_source=PurchaseSource.objects.get(name=Purchase_source)

                        if Stock.objects.filter(Bill_no=Bill_no,).exists():
                                already_uploaded.append(purchase_source.name)
                        else:
                            Stock.objects.create(
                                Material=Material,
                                Purity=Purity,
                                Nepali_date=Nepali_date,
                                Purchased_from=Purchased_from,
                                Purchase_source=purchase_source,
                                Total_weight=Total_weight,
                                Rate=Rate,
                                Grand_total=float(Grand_total),
                                Cash=float(Cash),
                                Bank=float(Bank),
                                Due=float(Due),
                                Bill_no=Bill_no,
                                Addition_gold_rate=Addition_gold_rate,
                                Notes=Notes,
                                status='approved'
                                    )
                            print(f"Created stock object for Purchase Source: {Purchase_source}, Purchased From: {Purchased_from}")

                if already_uploaded:
                    message = f"The following stocks are already uploaded: {', '.join(already_uploaded)}"
                    print("The following stocks are already uploaded")
                    return render(request, self.template_name, {'message': message,'stocks':stocks})


        return render(request, self.template_name,{
            'stock_upload_form': stock_upload_form,
            'stocks':stocks
        })


    def delete(self,request):
        id=json.loads(request.body)['id']
        stock=get_object_or_404(Stock,id=id)
        stock.status='deleted'
        stock.save()
        return redirect('stocks:stocks')

class StockCreateView(View):
    model=Stock
    permission_required='superuserstatus'
    template_name='stocks/stock_create.html'

    def get(self, request):
        stockform = StockForm()
        form=PurchaseSourceForm()
        return render(request, self.template_name, {'form': form,'stockform':stockform})

    def post(self,request):
        stockform=StockForm(self.request.POST,self.request.FILES or None)
        form=PurchaseSourceForm(self.request.POST,self.request.FILES or None)
        if stockform.is_valid():
            stock=stockform.save(commit=False)
            stock.status = 'completed'
            stock.save()
            create_transaction(stock)
            return redirect('stocks:goldstocks')

        if form.is_valid():
            purchase = form.save(commit=False)
            purchase.save()
            return redirect('stocks:stock-create')
        return render(request, self.template_name, {'form': form,'stockform':stockform})


class StockUpdateView(PermissionRequiredMixin, UpdateView):
    permission_required = 'superuserstatus'
    model = Stock
    form_class = StockForm
    template_name = 'stocks/stock_update.html'
    success_url = reverse_lazy('stocks:goldstocks')


    def form_valid(self, form):
        self.object = form.save(commit=False)
        self.object.save()
        return super().form_valid(form)


class StockDeletedListView(TemplateView):
    model=Stock
    context_object_name = 'deleted_stocks'
    permission_required='superuserstatus'
    template_name='stocks/deleted.html'

    def get(self, request, *args, **kwargs):
        deleted_stocks = Stock.objects.filter(status='deleted')
        queryset={'deleted_stocks':deleted_stocks}
        return render(request,self.template_name,queryset)

    def delete(self,request):
        id=json.loads(request.body)['id']
        print(id)
        stock=get_object_or_404(Stock,id=id)
        stock.delete()
        return redirect('stocks:stocks_deleted')

def restore_stock(request, pk):
    stock = Stock.objects.get(pk=pk)
    stock.status = 'approved'
    stock.save()
    return redirect('stocks:stocks_deleted')

class GoldOrnamentListView(PermissionRequiredMixin,TemplateView):
    permission_required='superuserstatus'
    template_name='stocks/stockornaments.html'
    
    def get(self,request, *args, **kwargs):
        form=StockOrnamentForm()
        stockornaments=StockOrnaments.objects.filter(status='approved')
        # totalweight=stocks.aggregate(total_weight_sum=Sum('Total_weight')).get('total_weight_sum') or 0
        # total=stocks.aggregate(total_sum=Sum('Grand_total')).get('total_sum') or 0
        queryset={'stockornaments':stockornaments,
                  'form':form,
                  }
        return render(request,self.template_name,queryset)


    def post(self, request,*args, **kwargs):
        stocks=Stock.objects.filter(status='approved').order_by('Nepali_date')
        stock_upload_form = StockUploadForm(request.POST, request.FILES)

class StockOrnamentCreateView(View):
    model=StockOrnaments
    context_object_name = 'stockornament'
    permission_required='superuserstatus'
    template_name='stocks/stockornament_create.html'

    def get(self, request):
        selected_ornaments=Ornaments.objects.filter(status='approved')
        stockornamentform = StockOrnamentForm()
        return render(request, self.template_name, {'stockornamentform':stockornamentform,'selected_ornaments':selected_ornaments})

    def post(self,request):
        stockornamentform=StockOrnamentForm(self.request.POST,self.request.FILES or None)
 
        if stockornamentform.is_valid():
            stockornament=stockornamentform.save(commit=False)
            stockornament.status='approved'
            stockornament.save()

            # Get the selected ornaments from the form
            selected_ornaments = stockornamentform.cleaned_data['selected_ornaments']

            
            # Add the selected ornaments to the Sales object
            for ornament in selected_ornaments:
                stockornament.selected_ornaments.add(ornament)

            # Calculate the total weight and save the object again
            stockornament.calculate_total_weight()
            stockornament.save()
            return redirect('stocks:ornamentstock')
        
        return render(request, self.template_name, {
            'stockornamentform': stockornamentform,
        })
               

class OrnamentCountView(PermissionRequiredMixin,TemplateView):
    permission_required='superuserstatus'
    template_name = 'ornaments/ornamentscount.html'

    def get(self, request, *args, **kwargs):
            form=OrnamentForm()
            main_categories_with_counts = MainCategory.objects.annotate(
                count_ornaments=Count('ornaments', filter=Q(ornaments__status='approved', ornaments__stock='AVAILABLE')),
                total_amount=Sum('ornaments__grand_total', filter=Q(ornaments__status='approved', ornaments__stock='AVAILABLE'))
                )
                
            ordered_with_counts = MainCategory.objects.annotate(count_ornaments=Count('ornaments', filter=Q(ornaments__status='approved', ornaments__stock='ORDERED')))
            # subcategories_with_counts = SubCategory.objects.annotate(count_ornaments=Count('ornaments', filter=Q(ornaments__status='approved', ornaments__stock='AVAILABLE')))

            context = {
                'form':form,
                'main_categories_with_counts': main_categories_with_counts,
                'ordered_with_counts': ordered_with_counts,
            }

            return render(request, self.template_name, context)

    def post(self,request):
            form=OrnamentForm(self.request.POST,self.request.FILES or None)
            if form.is_valid():
                ornament=form.save(commit=False)
                ornament.status='approved'
                ornament.save()


            return redirect('stocks:ornamentscount')

class CreateOrnamentView(View):
    def post(self, request, *args, **kwargs):
        form = OrnamentForm(request.POST, request.FILES)
        if form.is_valid():
            ornament = form.save(commit=False)
            ornament.status = 'approved'
            ornament.save()
            return redirect('stocks:ornamentscount')  # Redirect to the count view after successful submission
        # Handle form validation errors if needed
        return render(request, 'ornaments/create_ornament.html', {'form': form})  # Render a form with errors

    # You can also define a GET method if needed to display the form initially.
    

class OrnamentListView(PermissionRequiredMixin,TemplateView):
    model=Ornaments
    context_object_name = 'ornaments'
    permission_required='superuserstatus'
    template_name='ornaments/ornaments.html'


    def get(self,request, *args, **kwargs):
        form=OrnamentForm()
        ornament_upload_form = OrnamentUploadForm(request.POST, request.FILES)
        stock = self.request.GET.get('stock')
        search_query=self.request.GET.get('search')  # Get the search query from the request
        # Filter ornaments based on the selected main category
        selected_category_id = self.request.GET.get('main_category')

        if stock:
            ornaments=Ornaments.objects.filter(status='approved',stock=stock).order_by('-nepali_date')
        elif search_query:
            ornaments = Ornaments.objects.filter(Q(product_name__icontains=search_query) | Q(main_category__name__icontains=search_query),
                                                 status='approved',stock='AVAILABLE').order_by('-nepali_date')
        elif selected_category_id:
                selected_category = MainCategory.objects.get(pk=selected_category_id)
                ornaments = Ornaments.objects.filter(main_category=selected_category,status='approved',stock='AVAILABLE').order_by('-nepali_date')
        else:
            ornaments=Ornaments.objects.filter(status='approved',stock='AVAILABLE').order_by('-nepali_date')



        # ... rest of the code ...
        weight=ornaments.aggregate(weight_sum=Sum('weight')).get('weight_sum') or 0
        totaljarti=ornaments.aggregate(total_jarti_sum=Sum('jarti')).get('total_jarti_sum') or 0
        totalweight=ornaments.aggregate(total_weight_sum=Sum('total_weight')).get('total_weight_sum') or 0
        total=ornaments.aggregate(total_sum=Sum('grand_total')).get('total_sum') or 0
        due=ornaments.aggregate(due_sum=Sum('due')).get('due_sum') or 0
        ornaments_count = Ornaments.objects.filter(status='approved', stock='AVAILABLE').count()
        # Fetch all main categories
        main_categories = MainCategory.objects.all()

        # Pagination
        page = request.GET.get('page')
        paginator = Paginator(ornaments, 30)  # Display 10 ornaments per page
        ornaments = paginator.get_page(page)

        queryset={'ornaments':ornaments,
                  'ornament_upload_form':ornament_upload_form,
                  'form':form,
                  'weight':weight,
                  'totaljarti':totaljarti,
                  'totalweight':totalweight,
                  'total':total,
                  'due':due,
                  'main_categories':main_categories,
                  'ornaments_count':ornaments_count
                  }
        return render(request,self.template_name,queryset)

    def post(self, request,*args, **kwargs):
        ornaments=Ornaments.objects.filter(status='approved').order_by('nepali_date')
        ornament_upload_form = OrnamentUploadForm(request.POST, request.FILES)

        if  ornament_upload_form.is_valid():
            print("Success 0")
            excel_file = request.FILES['ornament_file']
            if excel_file.name.endswith('.xlsx'):
                workbook = openpyxl.load_workbook(excel_file)
                sheet = workbook.active
                for row in sheet.iter_rows(min_row=2, values_only=True):
                    print("Success 2")
                    material = row[0]
                    purity = row[1]
                    product_name=row[2]
                    weight=row[3]
                    jarti=row[4]
                    total_weight=row[5]
                    rate=row[6]
                    wages=row[7]
                    stones=row[8]
                    grand_total=row[9]
                    gold=row[10]
                    cash=row[11]
                    bank=row[12]
                    due=row[13]
                    purchased_from=row[14]
                    kaligar=row[15]
                    nepali_date=row[16]
                    category=row[17]

                    if row[0]:
                        if purchased_from:
                            try:
                                purchased_from_obj = PurchaseSource.objects.get(name=purchased_from)
                                purchased_from = purchased_from_obj
                            except PurchaseSource.DoesNotExist:
                                purchased_from = None

                        if kaligar:
                            try:
                                kaligar_obj = Kaligar.objects.get(name=kaligar)
                                kaligar = kaligar_obj
                            except Kaligar.DoesNotExist:
                                kaligar = None

                        if category:
                            try:
                                category_obj = MainCategory.objects.get(name=category)
                                print(category_obj)
                                main_category = category_obj
                            except MainCategory.DoesNotExist:
                                 # Category does not exist, so create a new one
                                with transaction.atomic():
                                    main_category = MainCategory.objects.create(name=category)
                        else:
                            main_category=None

                        Ornaments.objects.create(
                            material=material,
                            purity=purity,
                            main_category=main_category,
                            product_name=product_name,
                            weight=weight,
                            jarti=jarti,
                            total_weight=total_weight,
                            rate=rate,
                            wages=float(wages),
                            stones=float(stones),
                            grand_total=float(grand_total),
                            gold=gold,
                            cash=float(cash),
                            bank=bank,
                            due=due,
                            purchased_from=purchased_from,
                            kaligar=kaligar,
                            nepali_date=nepali_date,
                            status='approved'
                                    )
                        print(f"Created ornament object for Purchase Source: {product_name}, Purchased From: {kaligar}")


        return render(request, self.template_name,{
            'ornament_upload_form': ornament_upload_form,
            'ornaments':ornaments
        })


    def delete(self,request):
        id=json.loads(request.body)['id']
        print(id)
        ornament=get_object_or_404(Ornaments,id=id)
        ornament.status='deleted'
        ornament.save()
        return redirect('stocks:ornament')

class OrnamentCreateView(View):
    model=Ornaments
    context_object_name='ornaments'
    permission_required='superuserstatus'
    template_name='ornaments/ornament_create.html'

    def get(self, request):
        form = OrnamentForm()
        return render(request, self.template_name, {'form': form})

    def post(self,request):
            form=OrnamentForm(self.request.POST,self.request.FILES or None)
            if form.is_valid():
                ornament=form.save(commit=False)
                ornament.status='approved'
                ornament.save()


                return redirect('stocks:ornaments')

            return render(request, self.template_name, {'form': form})

class SalesOrnamentCreateView(View):
    model=Ornaments
    context_object_name='ornaments'
    permission_required='superuserstatus'

    def get(self, request):
        form = OrnamentForm()
        return render(request, self.template_name, {'form': form})

    def post(self,request):
            form=OrnamentForm(self.request.POST,self.request.FILES or None)
            if form.is_valid():
                ornament=form.save(commit=False)
                ornament.status='approved'
                ornament.save()


                return redirect('sales:sales-create')

            return render(request, self.template_name, {'form': form})
        
class OrnamentUpdateView(PermissionRequiredMixin, UpdateView):
    permission_required = 'superuserstatus'
    model = Ornaments
    form_class = OrnamentForm
    template_name = 'ornaments/ornament_update.html'
    success_url = reverse_lazy('stocks:ornaments')


    def form_valid(self, form):
        self.object = form.save(commit=False)
        self.object.save()
        return super().form_valid(form)

class OrnamentDeletedListView(TemplateView):
    model=Ornaments
    context_object_name = 'deleted_ornaments'
    permission_required='superuserstatus'
    template_name='ornaments/deleted.html'

    def get(self, request, *args, **kwargs):
        deleted_ornaments = Ornaments.objects.filter(status='deleted')
        queryset={'deleted_ornaments':deleted_ornaments}
        return render(request,self.template_name,queryset)

    def delete(self,request):
        id=json.loads(request.body)['id']
        print(id)
        ornament=get_object_or_404(Ornaments,id=id)
        ornament.delete()
        return redirect('stocks:ornaments_deleted')

def restore_ornament(request, pk):
    ornament = Ornaments.objects.get(pk=pk)
    ornament.status = 'approved'
    ornament.save()
    return redirect('stocks:ornaments_deleted')

