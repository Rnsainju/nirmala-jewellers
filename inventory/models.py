import random
import os
from django.db import models
from django.conf import settings
from django.shortcuts import reverse, redirect
from django.urls import reverse_lazy
from django.utils.text import slugify
from nepali_datetime_field.models import NepaliDateField
from transaction.models import Kaligar

def get_filename_ext(filepath):
    base_name=os.path.basename(filepath)
    name,ext=os.path.splitext(base_name)
    return name,ext

def upload_image_path(instance, filename):
    new_filename=random.randint(1,3910209312)
    name, ext= get_filename_ext(filename)
    final_filename='{new_filename}{ext}'.format(new_filename=new_filename,ext=ext)
    return "ornaments/{final_filename}".format(
            new_filename=new_filename,
            final_filename=final_filename
    )

class GoldRate(models.Model):
    GOLD_TYPE_CHOICES = (
        ('FINE GOLD (9999)', 'Fine Gold (9999)'),
        ('TEJABI GOLD', 'TEJABI GOLD'),  # Add more types as needed
    )

    gold_type = models.CharField(max_length=50, choices=GOLD_TYPE_CHOICES)
    unit = models.CharField(max_length=20)
    price = models.DecimalField(max_digits=10, decimal_places=2)
    nepalidate=NepaliDateField()
    
    def __str__(self):
        return f"{self.gold_type} - {self.unit} - {self.price}"
    
class OrnamentsManager(models.Manager):
    def featured(self):
        return self.get_queryset().filter(featured=True)

class PurchaseSource(models.Model):
    
    STATUS_CHOICES=(
        ('approved','Approved'),
        ('in-approved','In-approved'),
        ('deleted','Deleted'),
    )


    name=models.CharField(max_length=50, unique=True)
    pan=models.IntegerField(null=True, blank=True)
    status=models.CharField(max_length=50,choices=STATUS_CHOICES,default="approved")

    def __str__(self):
        return self.name

    def total_weight(self):
        list=Stock.objects.filter(status='completed',Purchase_source=self)
        total_weight=sum(item.Total_weight for item in list)
        return total_weight
    
    def total_amount(self):
        list=Stock.objects.filter(status='completed',Purchase_source=self)
        total_amount=sum(item.Grand_total for item in list)
        return total_amount

class MainCategory(models.Model):
    name = models.CharField(max_length=100)

    def __str__(self):
        return self.name

class SubCategory(models.Model):
    name = models.CharField(max_length=100)
    # main_category = models.ForeignKey(MainCategory, on_delete=models.CASCADE, related_name='subcategories')

    def __str__(self):
        return self.name
    
class Ornaments(models.Model):
    
    MATERIAL_CHOICES=(
        ('Gold','gold'),
        ('Silver','silver'),
        )

    PURITY_CHOICES=(
        ('24 CARAT','24-carat'),
        ('22 CARAT','22-carat'),
        ('18 CARAT','18-carat'),
        ('14 CARAT','14-carat')
        )   

    PAYMENT_CHOICES=(
        ('Cash','cash'),
        ('Transfer','transfer'),
        )

    STATUS_CHOICES=(
    ('approved','Approved'),
    ('in-approved','In-approved'),
    ('deleted','Deleted'),
    )

    PURCHASED_FROM=(
        ('Bank','bank'),
        ('Shops','shops'),
        ('Customer','customer'),
        ('Vendor','vendor')
    )
    
    STOCK_CHOICES=(
    ('IN-AVAILABLE','IN-AVAILABLE'),
    ('AVAILABLE','AVAILABLE'),
    ('ORDERED','ORDERED'),
    ('SOLD','SOLD'),
    ('DESTROYED','DESTROYED'),
    )
    
    material=models.CharField(max_length=50,choices=MATERIAL_CHOICES)
    purity=models.CharField(max_length=50,choices=PURITY_CHOICES)
    # slug = models.SlugField(max_length=100, unique=True, blank=True)
    main_category=models.ForeignKey(MainCategory, on_delete=models.CASCADE, null=True)
    sub_category=models.ForeignKey(SubCategory, on_delete=models.CASCADE, null=True)
    product_name=models.CharField(max_length=50,blank=True,unique=False)
    weight=models.FloatField(null=True, blank=True, default=None)
    jarti=models.FloatField(null=True, blank=True, default=None)
    total_weight=models.FloatField(null=True, blank=True, default=0)
    rate=models.FloatField()
    wages=models.FloatField(null=True, blank=True, default=None)
    stones=models.FloatField(null=True, blank=True, default=None)
    grand_total=models.FloatField(null=True, blank=True, default=None)
    gold=models.FloatField(null=True, blank=True, default=None)
    cash=models.FloatField(null=True, blank=True, default=None)
    bank=models.FloatField(null=True, blank=True, default=None)
    due=models.FloatField(null=True, blank=True, default=None)
    purchased_from=models.ForeignKey(PurchaseSource, on_delete=models.CASCADE, null=True,blank=True)
    kaligar=models.ForeignKey(Kaligar, on_delete=models.CASCADE, null=True,blank=True)
    image=models.ImageField(upload_to=upload_image_path, null=True, blank=True)
    nepali_date=NepaliDateField()
    status=models.CharField(max_length=50,choices=STATUS_CHOICES,default="in-approved")
    stock=models.CharField(max_length=50,choices=STOCK_CHOICES,default="AVAILABLE")
    
    def __str__(self):
        return f"{self.product_name} - Weight: {self.weight} - Purity: {self.purity}"

    def save(self, *args, **kwargs):
        # Combine product_name, weight, and kaligar name to create the slug
        # slug_str = f"{self.product_name}-{self.weight}-{self.kaligar.name}"
        # self.slug = slugify(slug_str)[:100]
        if self.jarti:
            self.total_weight=self.weight+self.jarti
        else:
            self.total_weight = self.weight
        total=(self.total_weight/11.664)*self.rate
        if self.wages:
            total += self.wages
        if self.stones:
            total += self.stones
        self.grand_total = total
        if self.cash is not None or self.bank is not None:
            self.due=self.grand_total-(self.cash or 0) -(self.bank or 0)
        super(Ornaments,self).save(*args,**kwargs)

class StockOrnaments(models.Model):
    
    
    STATUS_CHOICES=(
    ('approved','Approved'),
    ('in-approved','In-approved'),
    ('deleted','Deleted'),
    )


    bill_no=models.IntegerField(null=True, blank=True)
    nepali_date=NepaliDateField(null=True, blank=True, default=None)
    selected_ornaments = models.ManyToManyField(Ornaments, blank=True)
    purchase_source=models.ForeignKey(PurchaseSource, on_delete=models.CASCADE, null=True, blank=True)
    total_weight=models.FloatField(null=True, blank=True,)
    own_gold=models.FloatField(null=True, blank=True)
    discounts=models.IntegerField(null=True, blank=True)
    grand_total=models.FloatField(null=True, blank=True)
    cash=models.FloatField(default=None,null=True, blank=True)
    bank=models.FloatField(default=None,null=True, blank=True)
    due=models.FloatField(null=True, blank=True, default=None)
    notes=models.TextField(null=True, blank=True,)
    status=models.CharField(max_length=50,choices=STATUS_CHOICES,null=True, blank=True)
    
    def __str__(self):
        return f"Purchase #{self.pk} - {self.purchase_source.name}"

    def calculate_total_weight(self):
        grand_total = 0.0
        total_weight=0.0
        discounts = self.discounts or 0.0

        # Iterate through the selected ornaments and calculate the total weight
        for ornament in self.selected_ornaments.all():
            total_weight += ornament.total_weight
            grand_total += ornament.grand_total
        
        self.total_weight=total_weight
        self.grand_total=grand_total-discounts
        total=self.grand_total
        self.due=total-(self.own_gold or 0)-(self.cash or 0) -(self.bank or 0)
        self.save()
          
            
class Stock(models.Model):
    STATUS_CHOICES=(
    ('approved','Approved'),
    ('in-approved','In-approved'),
    ('completed','Completed'),
    ('deleted','Deleted'),
    )

    MATERIAL_CHOICES=(
        ('Gold','gold'),
        ('Silver','silver'),
        )
    
    PURITY_CHOICES=(
        ('24 CARAT','24-carat'),
        ('22 CARAT','22-carat'),
        ('18 CARAT','18-carat')
        )
    
    PURCHASED_FROM=(
        ('Bank','bank'),
        ('Shops','shops'),
        ('Customer','customer'),
        ('Vendor','vendor')
        )
    
    CITY_CHOICES=(
        ('KTM','ktm'),
        ('DHN','dhn'),
        )

    Material=models.CharField(max_length=50,choices=MATERIAL_CHOICES)
    Purity=models.CharField(max_length=50,choices=PURITY_CHOICES)
    Nepali_date=NepaliDateField()
    Purchased_from=models.CharField(max_length=50,choices=PURCHASED_FROM,default="bank")
    Purchase_source=models.ForeignKey(PurchaseSource, on_delete=models.CASCADE, null=True, blank=True)
    Total_weight=models.FloatField()
    Rate=models.FloatField()
    Grand_total=models.FloatField(null=True, blank=True, default=None)
    Cash=models.FloatField(null=True, blank=True, default=None)
    Bank=models.FloatField(null=True, blank=True, default=None)
    Due=models.FloatField(null=True, blank=True, default=None)
    Bill_no=models.IntegerField(null=True, blank=True, default=None)
    Addition_gold_rate=models.FloatField(blank=True,default=0)
    Notes=models.TextField(blank=True)
    status=models.CharField(max_length=50,choices=STATUS_CHOICES,default="in-approved")

    def __str__(self):
        return self.Purchased_from

    def save(self, *args, **kwargs):
        self.Grand_total=(self.Total_weight/11.664)*self.Rate
        if self.Cash is not None or self.Bank is not None:
            self.Due=self.Grand_total-(self.Cash or 0) -(self.Bank or 0)
        elif self.Addition_gold_rate:
            self.Grand_total=(self.Rate+self.Addition_gold_rate)*(self.Total_weight/10)
        super().save(*args,**kwargs)
       
    def total_24caratgoldweight(self):
        list=Stock.objects.filter(status='approved').exclude(Purchased_from='Customer')
        total_24caratgoldweight=0
        for item in list:
            total_24caratgoldweight += item.Total_weight
        
        return total_24caratgoldweight
        
    def total_mixgoldweight(self):
        list=Stock.objects.filter(status='approved',Purchased_from='Customer')
        total_mixgoldweight=0
        for item in list:
            total_mixgoldweight += item.Total_weight
        
        return total_mixgoldweight


    