from django.contrib import admin
from .models import Stock, StockOrnaments,Ornaments,PurchaseSource, MainCategory, SubCategory, GoldRate

admin.site.register(GoldRate)
admin.site.register(PurchaseSource)
admin.site.register(Stock)
admin.site.register(StockOrnaments)
admin.site.register(Ornaments)
admin.site.register(MainCategory)
admin.site.register(SubCategory)
